// MANAGES  THE SERIAL INTERFACE VIA THE USB PORT

void serialControl() {
  for (uint8_t p = 0; p < SC_TOF; p++) { // reset all the int registers
    readFromMAX(p, 0xFE);  delay(5);
    intReg_MAX[p] = 0;
  }
  Serial.println("");
  Serial.println("******** MAIN MENU ********");
  Serial.println("Enter 'm' to proceed to measurement menu");
  Serial.println("Enter 'c' to proceed to configuration menu");
  Serial.println("Enter 't' to generate spoofed TOF data (currently disabled)");
  Serial.println("Enter 's' to test SPI link to all MAX chips");
  Serial.println("Enter 'x' to disable serial comms until reset");
  Serial.println("Enter 'r' to reset");
  Serial.println("");
  uint8_t opcode = serialReadChar();

  switch (opcode) {
    case 'm': // measurement menu
      measuremtMenu();
      break;

    case 'c':  // CONFIGURATION MENU
      configurationMenu();
      break;

    case 't':
      break;

    case 'x': // disable serial comms until reset
      Serial.println("Serial control terminated");
      bSerialControl = LOW;
      break;

    case 'r':  // perform hard then soft reset of all MAX chips
      resetMAX(HIGH);
      delay(100);
      resetMAX(LOW);
      break;

    case 's': // test SPI to all MAX35 chips
      testMAXSPI();
      break;

    default:
      Serial.println("Invalid input. Try again.");
      serialControl();
  }
}


void measuremtMenu() {
  Serial.println();
  Serial.println("Select transducer pair to test, 1, 2, 3, 4, or 5 to maintain default active channels");
  uint8_t p = serialReadInt() - 1;

  Serial.println("");
  Serial.println("Select measurement type  "); // pulse direction
  Serial.print("1  ");  Serial.println("Upstream");
  Serial.print("2  ");  Serial.println("Downstream");
  Serial.print("3  ");  Serial.println("Differential");
  Serial.print("4  ");  Serial.println("Full Differential");
  Serial.println("");
  uint8_t option = serialReadInt();

  Serial.println();
  Serial.println("How many TOF measurements? 0 = indefinite measurements"); // count
  uint16_t count = serialReadInt();

  if (option == 4) { // full differential measurement
    boolean  USTenable_Backup[SC_TOF];
    for (uint8_t i = 0; i < SC_TOF; i++)
      USTenable_Backup[i] = USTenable[i];

    if (p != 4) {
      memset(USTenable, 0, 1 * SC_TOF); // just measure/calibrate one pair at a time.
      USTenable[p] = HIGH;
    }
    for (int i = 0; i < count; i++) {
      measureFlow();
      //printEEPROM();
    }
    for (uint8_t i = 0; i < SC_TOF; i++)
      USTenable[i] = USTenable_Backup[i];
    serialControl();
  }


  Serial.println("ToF Measurement in progress... ");
  if (count == 0) { // count = 0 means measure indefinitely
    while (true) // infinite loop
      simpleTOF(option, p, 65535);
  }
  else
    simpleTOF(option, p, count);
  serialControl();
}



void configurationMenu() {
  Serial.println();
  Serial.print("1  ");  Serial.println("Change pulse voltage");
  Serial.print("2  ");  Serial.println("Change recieve gain");
  Serial.print("3  ");  Serial.println("differential or single ended");
  Serial.print("4  ");  Serial.println("Change pulse cycle count");
  Serial.print("5  ");  Serial.println("Change ToF accepted range");
  Serial.print("6  ");  Serial.println("Change switching regulator stabilisation time.");
  Serial.print("7  ");  Serial.println("View and check configuration registers");
  Serial.println();

  opcode = serialReadInt();

  switch (opcode) {
    case 1: // Change pulse voltage
      //serial_pulse_voltage();
      break;

    case 2: // change recieve gain
      //serial_amp_gain();
      break;

    case 3: // select differential or single ended
      //      Serial.println();  Serial.println("Enter 0 for single ended and 1 for differential");
      //      value = serialReadInt();
      //      while (set_diff_or_single(value) == HIGH) {}
      break;

    case 4: // set pulse cycle size
      Serial.println();  Serial.println("enter the desired pulse cycle size from 1 to 127");
      pulseLength = serialReadInt();
      Serial.print("New pulse length is ");  Serial.print(pulseLength);
      break;

    case 5: // change ToF valid range
      Serial.print("Current lower bound in us is ");  Serial.println(expectedTOF - rxWindow);
      Serial.print("Current upper bound in us is ");  Serial.println(expectedTOF + rxWindow);
      Serial.println("Enter expected TOF transit time in microseconds");
      expectedTOF = serialReadInt();
      Serial.println("Enter RX window size in microseconds");
      rxWindow = serialReadInt();
      serialControl();
      break;

    case 6:
      //serial_switcher_stabilisation(); not currently in use
      break;

    case 7:
      Serial.println();
      printAndTest_MAXconfig(0);
      for (int p = 0; p < SC_TOF; p++) {
        if (validate_MAXconfig() == HIGH) {
          Serial.println("Config issue... Checking SPI link");
          testMAXSPI();
        }
      }
      serialControl();
      break;

    default:
      Serial.println("Invalid input. Try again.");
      serialControl();
  }
}


void serial_pulse_voltage() {  // serial menu for pulse voltage
  Serial.println();
  Serial.println("Table of voltage leFlowVel_mss - Enter 1 to 12");
  Serial.println("1  = 9.0V");
  Serial.println("2  = 10.8V");
  Serial.println("3  = 12.6V");
  Serial.println("4  = 15.0V");
  Serial.println("5  = 16.8V");
  Serial.println("6  = 19.2V");
  Serial.println("7  = 21.0V");
  Serial.println("8  = 22.8V");
  Serial.println("9  = 25.2V");
  Serial.println("10 = 27.0V");
  Serial.println("11 = 28.8V");
  Serial.println("12 = 30.6V");
  Serial.println();
  uint8_t value = serialReadInt();
  if (value <= 12 && value >= 1)
    pulseVoltage = value;
  else
    Serial.println("Invalid voltage level!");
}


//void serial_amp_gain() { // this internal amplifier is currently not in use.
//  Serial.println();
//  Serial.println("Table of reciever gain leFlowVel_mss (voltage ratio)");
//  Serial.println("1  = 3.16");
//  Serial.println("2  = 3.39");
//  Serial.println("3  = 4.3");
//  Serial.println("4  = 5.01");
//  Serial.println("5  = 5.83");
//  Serial.println("6  = 6.8");
//  Serial.println("7  = 7.93");
//  Serial.println("8  = 9.32");
//  Serial.println("9  = 10.76");
//  Serial.println("10 = 12.55");
//  Serial.println("11 = 14.62");
//  Serial.println("12 = 17.04");
//  Serial.println("13 = 19.86");
//  Serial.println("14 = 23.15");
//  Serial.println("15 = 26.98");
//  Serial.println("16 = 31.44");
//  Serial.println();
//  value = serialReadInt();
//}


//void serial_switcher_stabilisation() {  // NOT IN USE
//  Serial.println();
//  Serial.println("Switcher stabilisation time");
//  Serial.println("1  = 64 us");
//  Serial.println("2  = 128 us");
//  Serial.println("3  = 192 us");
//  Serial.println("4  = 256 us");
//  Serial.println("5  = 320 us");
//  Serial.println("6  = 384 us");
//  Serial.println("7  = 473 us");
//  Serial.println("8  = 512 us");
//  Serial.println("9  = 768 us");
//  Serial.println("10 = 1.20 ms");
//  Serial.println("11 = 1.25 ms");
//  Serial.println("12 = 1.50 ms");
//  Serial.println("13 = 2.05 ms");
//  Serial.println("14 = 4.10 ms");
//  Serial.println("15 = 8.19 ms");
//  Serial.println("16 = 16.4 ms");
//  Serial.println();
//  value = serialReadInt();
//}


uint8_t serialReadChar() {
  uint8_t opcode = 0;
  Serial.println();
  Serial.println("Awaiting input... ");
  Serial.println();
  while (Serial.available() == 0) {
    delay(100); // wait until serial input is recieved
  }
  opcode = Serial.read(); // read char

  while (Serial.available() > 0) { // ensure serial buffer is clear
    Serial.read();
    delay(100);
  }
  return opcode;
}


uint16_t serialReadInt() {
  uint16_t value = 0;
  Serial.println();
  Serial.println("Awaiting input... ");
  while (Serial.available() == 0) {
    delay(100); // wait until serial input is recieved
  }
  value = Serial.parseInt(); // read int

  while (Serial.available() > 0) { // ensure serial buffer is clear
    Serial.read();
    delay(100);
  }
  return value;
}



void simpleTOF(uint8_t option, uint8_t p, uint16_t count) { // p is the transducer pair being tested
  double TOF = 0;
  uint32_t TOF_logUP[count] = {0}; // in ps
  uint32_t TOF_logDN[count] = {0}; // in ps
  double TOF_logDiff[count] = {0}; // in ns with decimals

  for (int i = 0; i < count; i++) {
    Serial.println();  Serial.print("Measurement ");  Serial.print(i); Serial.print(", ToF pair "); Serial.print(p + 1); Serial.println(" in progress");
    readFromMAX(p, 0xFE);   delay(5);   // Reads MAX_INT register to reset it
    intReg_MAX[p] = 0;
    bTOFcompleteFlag[p] = LOW; // reset flag which tracks completed ToF measurements
    writeToMAX(p, (option - 1), 0, HIGH); // TOF command.
    awaitingTOF(); // wait for measurement result
    switch (option) {
      case 1:  // upstream measurements
        TOF = (double) 250 * (readFromMAX(p, 0xD1) + (double)readFromMAX(p, 0xD2) / 65536);
        TOF_logUP[i] = (uint32_t) 1000 * TOF;  // save as integer pico seconds
        Serial.print("TOFUP: ");  Serial.print(TOF / 1000);  Serial.println(" us");
        break;
      case 2: { // downstream measurements
          TOF = (double) 250 * (readFromMAX(p, 0xE0) + (double)readFromMAX(p, 0xE1) / 65536);
          TOF_logDN[i] = (uint32_t) 1000 * TOF;  // save as integer pico seconds
          Serial.print("TOFDN: ");  Serial.print(TOF / 1000);  Serial.println(" us");
        }
      case 3:  // difference measurements
        TOF = (double) 250 * (readFromMAX(p, 0xD1) + (double)readFromMAX(p, 0xD2) / 65536);
        TOF_logUP[i] = (uint32_t) 1000 * TOF;  // save as integer pico seconds
        Serial.print("TOFUP: ");  Serial.print(TOF / 1000);  Serial.println(" us");
        TOF = (double) 250 * (readFromMAX(p, 0xE0) + (double)readFromMAX(p, 0xE1) / 65536);
        TOF_logDN[i] = (uint32_t) 1000 * TOF;  // save as integer pico seconds
        Serial.print("TOFDN: ");  Serial.print(TOF / 1000);  Serial.println(" us");
        TOF = (double)250 * (readFromMAX_signed(p, 0xE2) + (double)readFromMAX(p, 0xE3) / 65536);
        TOF_logDiff[i] = TOF;
        Serial.print("TOFDiff: ");  Serial.print(TOF);  Serial.println(" ns");
        break;
      default:
        Serial.println("Invalid instructions send to TOF function");
        return;
    }
  }

  switch (option) {
    case 1:
      Serial.println("Upstream TOF results (ps)");
      for (int i = 0; i < count; i++)
        Serial.println(TOF_logUP[i]);
      Serial.print("Standard deviation: ");  Serial.print(standardDev(TOF_logUP, m) / 1000);  Serial.println(" ns");
      break;
    case 2:
      Serial.println("Downstream TOF results (us)");
      for (int i = 0; i < count; i++)
        Serial.println(TOF_logUP[i] / 1000000);
      Serial.print("Standard deviation: ");  Serial.print(standardDev(TOF_logDN, m) / 1000);  Serial.println(" ns");
      break;
    case 3:
      Serial.println("Upstream TOF results (ns)");
      for (int i = 0; i < count; i++)
        Serial.println(TOF_logUP[i] / 1000);
      Serial.println("Downstream TOF results (ns)");
      for (int i = 0; i < count; i++)
        Serial.println(TOF_logDN[i] / 1000);
      Serial.println("Difference TOF results (ns)");
      for (int i = 0; i < count; i++)
        Serial.println(TOF_logDiff[i]);
      Serial.print("Standard deviation UP:   ");  Serial.print(standardDev(TOF_logUP, m) / 1000);  Serial.println(" ns");
      Serial.print("Standard deviation DN:   ");  Serial.print(standardDev(TOF_logDN, m) / 1000);  Serial.println(" ns");
      Serial.print("Standard deviation Diff: ");  Serial.print(standardDev(TOF_logUP, m) / 1000);  Serial.println(" ns");
      break;
  }
}
