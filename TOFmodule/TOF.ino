

void measureTOF() {
#ifdef TOFDebug
  Serial.println();  Serial.print("Measuring Time-of-Flight. Budget is "); Serial.print(aveTimeTOF);  Serial.println(" ms...");
#endif

  if (pulseVoltage > HBpulseVoltage) {
#ifdef TOFDebug
    Serial.println();  Serial.println();
    Serial.println("!! PULSE VOLTAGE EXCEEDS EXPLOSIVE ATMOSPHERE COMPLIANCE LEVEL !!");
    Serial.print(" SETTING TO SAFE LEVEL OF ");  Serial.println(HBpulseVoltage);
#endif
    pulseVoltage = HBpulseVoltage;
  }

  m = 0;   // counts total number of measurements global variable
  uint16_t f[4] = {0}; // fixed filtered measurements count. Unique for each channel.
  uint16_t g[4] = {0}; // good measurements count. Values used for final average. Unique for each channel.


  for (uint8_t p = 0; p < SC_TOF; p++) {
    clearResults(p);
#ifdef TOFDebug
    Serial.print("Channel ");  Serial.print(p + 1);
    if (USTenable[p] == HIGH)
      Serial.println(" is ACTIVE. ");
    else
      Serial.println(" is INACTIVE. ");
#endif
  }

#ifdef TOFDebug
  Serial.println();
#endif

  if (activePairs < 1) {
#ifdef TOFDebug
    Serial.println("!! ERROR - NO CHANNELS ARE ACTIVE !!");
#endif
    bitWrite(statusReg, TOFissue, HIGH); // set modules interrupt TOF bit low. will go HIGH upon completion.
    digitalWrite(pINT_module, LOW); // assert int pin
    return;
  }

  uint32_t timer = millis(); // To calc total measurement duration
  while ((millis() - timer) < aveTimeTOF) { // multiple measurements per averaged result
    // STEP 1 - COMMAND MEASUREMENT START ON max35 CHIPS
    // reset the int reg, and command a measure for each channel
#ifndef simulation_mode
    for (uint8_t p = 0; p < SC_TOF; p++) {
      if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.
      intReg_MAX[p] = readFromMAX(p, 0xFE);  // read MAX int register to reset it.
      intReg_MAX[p] = 0;
      bTOFcompleteFlag[p] = LOW; // reset flag which tracks completed ToF measurements
      bTOFtimeoutFlag[p] = LOW; // reset flag that tracks timeouts
      responded[p] = LOW;        // reset response tracker
      delayMicroseconds(launch_delay);
      writeToMAX(p, 0x02, 0, HIGH);  // TOF_diff command. measures up and downstream.
    }

#ifdef TOFDeepDebug
    Serial.println(); Serial.println();
    Serial.print("Measurement ");  Serial.print(m);  Serial.print(" - All pulses launched... ");
#endif
#endif

    // STEP 2 - IDENTIFY NEXT CHANNEL READY TO READ TOF DATA FROM
    int8_t p = 0; // TOF pair currently being read from
    // ITERATE OVER PAIRS
    for (uint8_t i = 0; i < SC_TOF; i++) {
#ifdef simulation_mode
      if (USTenable[i] == HIGH) { // if the next transducer is enabled, use it. else skip that transducer.
      p = i;
      delay(2);
      }
      else {
        continue;
      }
#else
      p = awaitingTOF();  // wait for measurement to finish, and identify the pair that finished
      if (p == -1) continue; // -1 means this specific channel timed out via MAX35. Move to next channel.
      if (p == -2) break; // -2 means nobody responded. skip to next full cycle.
#endif


      // STEP 3 - READ RESULTS REGISTERS, PRINT, AND STORE
      // max fractional value should be FFFF=250ns. Whole value should be multiples of 250ns.
      double TOFUP_ns = 0;
      double TOFDN_ns = 0;
      double TOFdiff_ns = 0;

#ifdef simulation_mode
      simulateMAX35(p, &TOFUP_ns, &TOFDN_ns, &TOFdiff_ns);
#else
      TOFUP_ns   = (double)250 * (readFromMAX(p, 0xD1) + (double)readFromMAX(p, 0xD2) / 65536);
      TOFDN_ns   = (double)250 * (readFromMAX(p, 0xE0) + (double)readFromMAX(p, 0xE1) / 65536);
      TOFdiff_ns = (double)250 * (readFromMAX_signed(p, 0xE2) + (double)readFromMAX(p, 0xE3) / 65536);
#endif

#ifdef TOFDeepDebug
      Serial.print("   TOFUP ");   Serial.print(TOFUP_ns / 1000);    Serial.print(" us");
      Serial.print("   TOFDN ");   Serial.print(TOFDN_ns / 1000);    Serial.print(" us");
      Serial.print("   TOFDiff "); Serial.print(TOFdiff_ns);         Serial.print(" ns");
#endif
      if (m < 256) { // don't log data that would exceed array max size
        TOF[p].UP[m]      = (uint32_t) 1000 * TOFUP_ns;  // store uptime in ps
        TOF[p].DN[m]      = (uint32_t) 1000 * TOFDN_ns;  // store downtime in ps
      }
      TOF[p].aveUPraw += (uint64_t) 1000 * TOFUP_ns; // accumulate for average
      TOF[p].aveDNraw += (uint64_t) 1000 * TOFDN_ns; // accumulate for average

      // checking to see if the registers are still holding default values (they shouldn't be if they responded without a timeout!)
      if (TOFUP_ns == 4294967296 && TOFDN_ns == 4294967296 && TOFdiff_ns == 16777216) {
        TOF_issue(p);
      }

      // STEP 4 - CHECK, CORRECT, AND FILTER, results
      TOFdiff_ns = correctTOF(TOFdiff_ns); // attempt to correct integer wavelength skips
      // check data
      // ensure uptime and downtime are both within reasonable limits
      if ((TOFUP_ns >= 1000 * (expectedTOF - rxWindow)) && (TOFUP_ns <= 1000 * (expectedTOF + rxWindow)) && (TOFDN_ns >= 1000 * (expectedTOF - rxWindow)) && (TOFDN_ns <= 1000 * (expectedTOF + rxWindow))) {
        if ( abs(TOFdiff_ns) <  TOFlimit ) { // is TOFdifference sensible?
          TOF[p].diffLogF[f[p]] = 1000 * TOFdiff_ns; // store time difference in ps
          //Serial.print("   ");  Serial.print(TOF[p].diffLogF[f[p]]/1000);  Serial.print(" ns passed first filter. f is ");  Serial.println(f[p]);
          
          TOF[p].aveUPwinF += (uint64_t) 1000 * TOFUP_ns; // accumulate for average
          TOF[p].aveDNwinF += (uint64_t) 1000 * TOFDN_ns; // accumulate for average
          TOF[p].UPfilterLog[f[p]] = TOF[p].aveUPwinF;    // log used to calculate average after SD filter
          TOF[p].DNfilterLog[f[p]] = TOF[p].aveUPwinF;    // log used to calculate average after SD filter
          
          f[p]++; // increment fine/filtered counter
        }
      }
    } // READING SINGLE MEASUREMENT FROM EACH CHANNEL
    m++; // increment total number of measurements
  } // WHILE LOOP TO TAKE MULTIPLE MEASUREMENTS FOR AVERAGE



  // STEP 5 - OUTLIER REMOVAL
  // CALCULATE STANDARD DEVIATIONS. SAVE THEM TO resULTS STRUCTURE

  float TOFdiffAve_ns[SC_TOF] = {0}; // average filtered TOFdiff for SD threshold removal
  for (uint8_t p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.

    TOF[p].successRatio = (uint8_t) 100 * f[p] / m; // percentage of measurements that didn't time out and returned sensible values

    TOF[p].UP_SD   = standardDev(TOF[p].UP, m);
    TOF[p].DN_SD   = standardDev(TOF[p].DN, m);
    TOF[p].diffSD  = standardDev_signed(TOF[p].diffLogF, f[p]);


    for (uint16_t i = 0; i < f[p]; i++) { // average for each channel (filtered TOFdiff)
      TOFdiffAve_ns[p] = TOFdiffAve_ns[p] + (float)(TOF[p].diffLogF[i] / f[p]); // calculate average
    }

    // removal any points that lay a certain number of SDs away from the dataset mean
    float SD_Lbound =  TOFdiffAve_ns[p] - TOF[p].diffSD * SD_threshold / 10; //divide by 10 for SD (stored in tenths)
    float SD_Hbound =  TOFdiffAve_ns[p] + TOF[p].diffSD * SD_threshold / 10;

#ifdef TOFDebug
    Serial.println();
    Serial.print("Channel ");  Serial.print(p + 1);  Serial.print(", ");
    Serial.print(f[p]);  Serial.print(" point TOFdiff-ave = ");
    Serial.print(TOFdiffAve_ns[p] / 1000);  Serial.print(" ns. ");
    Serial.print(" SD = ");  Serial.print((float)TOF[p].diffSD / 1000, 2); Serial.print(" ns.  ");
    Serial.print("SD-L-bound = ");  Serial.print(SD_Lbound / 1000); Serial.print(" ns. ");
    Serial.print("SD-H-bound = ");  Serial.print(SD_Hbound / 1000); Serial.println(" ns. ");
#endif

    for (uint16_t i = 0; i < f[p]; i++) {  // SD threshold detection / outlier removal.
      if ( (TOF[p].diffLogF[i] <= SD_Hbound) && (TOF[p].diffLogF[i] >= SD_Lbound) ) { // less than higher bound greater than lower bound
        TOF[p].diffLogF[g[p]] = TOF[p].diffLogF[i];  // if the measurement passes then log it
        //Serial.print(TOF[p].diffLogF[g[p]] / 1000);  Serial.print(" ns passed SD. i=");  Serial.print(i);  Serial.print(", g=");    Serial.println(g[p]);
        
        TOF[p].aveUPallF = TOF[p].UPfilterLog[f[p]]; // accumulate for time average
        TOF[p].aveDNallF = TOF[p].DNfilterLog[f[p]]; // accumulate for time average
        
        g[p]++; // good measurement counter
      }
    }


    // STEP 6 - SAVING THE MEASUREMENT IN PS AND FLAG COMPLETE TO I2C
    double HmeanDenominator = 0;
    for (uint16_t i = 0; i < g[p]; i++) { // iterate and average over all of the recent good diff measurements
      if (aveMethod == 0) {
        TOF[p].diff = (int32_t) TOF[p].diff + (TOF[p].diffLogF[i] / g[p]); // saving arithmetic mean as integer picoseconds
      }
      if (aveMethod == 1) {
        HmeanDenominator = HmeanDenominator + (double)1 / TOF[p].diffLogF[i];
      }
    }
    if (aveMethod == 1) {
      TOF[p].diff = g[p] / HmeanDenominator; // save harmonic mean in ps
    }

    TOF[p].diff = TOF[p].diff - TOF[p].baseline; // applying fixed baseline

  } // iterate over pairs

// STEP 7 NEW IN 1.04 GET ALL TIME AVERAGES
  // do divisions for all time averages 
  Serial.println();
for (uint8_t p = 0; p < SC_TOF; p++) {
  Serial.print("pair "); Serial.print(p);
      TOF[p].aveUPraw = TOF[p].aveUPraw / m; // get average
      TOF[p].aveDNraw = TOF[p].aveDNraw / m; // get average
      
      TOF[p].aveUPwinF = TOF[p].aveUPwinF / f[p]; // get average
      TOF[p].aveDNwinF = TOF[p].aveUPwinF / f[p]; // get average
      
      TOF[p].aveUPallF = TOF[p].aveUPallF / g[p]; // get average
      TOF[p].aveDNallF = TOF[p].aveUPallF / g[p]; // get average

      Serial.print("- Ave RAW time UP: "); Serial.print((float)TOF[p].aveUPraw/1000000,3); Serial.print("us, DN: ");  Serial.print((float)TOF[p].aveDNraw/1000000,3); Serial.print("us");

}

#ifdef TOFDebug
  char result[200];
  sprintf(result, "%hu Total measurements, ave-all-channel time of %hu ms. Success Rates per channel are, %u %%,  %u %%,  %u %%,  %u %%", m * activePairs, aveTimeTOF / m, TOF[0].successRatio, TOF[1].successRatio, TOF[2].successRatio, TOF[3].successRatio);
  Serial.println(result);
#endif

  dataDump_counter++;

} //****************************** END OF MEASURE TOF ******************************


// WAITS FOR A MEASUREMENT TO FINISH AND RETURNS THE CHANNEL WHICH resPONDED
int8_t awaitingTOF() {
  uint32_t timeout = micros();
  while ((micros() - timeout) < (1000 * timeoutLimit)) {
    delayMicroseconds(10);
    for (uint8_t p = 0; p < SC_TOF; p++) { // checking each pair.
      if (responded[p] == HIGH) continue; // skip any pairs that have already responded this cycle
      if ((bTOFcompleteFlag[p] == HIGH) && (bTOFtimeoutFlag[p] == LOW)) {  // measurement done (12) without a timeout (15)
#ifdef TOFDeepDebug
        Serial.println();
        Serial.print("Channel ");  Serial.print(p + 1);  Serial.print(" responded.  ");
        Serial.print("Waited for ");  Serial.print((float)(micros() - timeout) / 1000, 2);  Serial.print(" ms   ");
#endif
        responded[p] = HIGH;
        bTOFcompleteFlag[p] = LOW; // measurement identified, so reset the flag.
        timeoutCounter[p] = 0; // reset the counter on every single completed measurement
        return p; // return the pair that just flagged a measurement complete
      }

      if ( bTOFtimeoutFlag[p] == HIGH ) { // if MAX35 times out then return error
#ifdef TOFDeepDebug
        Serial.println();  Serial.print("! TOF MEASUREMENT TIMEOUT 1 (From MAX35) - Ch "); Serial.print(p + 1);
#endif
        responded[p] = HIGH;
        return p; // return the pair that just flagged a measurement complete (well, a timeout)
      }
    }
  }
#ifdef TOFDeepDebug
  Serial.print("! TOF MEASUREMENT TIMEOUT 2 (");  Serial.print((float)(micros() - timeout) / 1000, 2);  Serial.println(" milliseconds elapsed)");
#endif
  for (uint8_t p = 0; p < SC_TOF; p++) {
    if ((responded[p] == LOW) && (USTenable[p] == HIGH)) {
#ifdef TOFDeepDebug
      Serial.print(" Channel ");  Serial.print(p + 1); Serial.println(" didn't respond! ");
#endif
      TOF_issue(p);
    }
  }
  return -2;
}


//boolean TOF_issue(uint8_t p) {
void TOF_issue(uint8_t p) {
  timeoutCounter[p]++;
  Serial.println("!!!! CHANNEL DIDN'T RESPOND OR RESULTS REGISTERS DEFAULTED !!!!");
  Serial.print(timeoutCounter[p]);  Serial.print(" consecutive non-responses channel ");  Serial.println(p + 1);

  TOF[p].timeoutC++; // accumulating for additional metrics added in V1.04


  if (timeoutCounter[p] >= timeoutCountLimit) { // many consecutive fails for any reason prompts a reset
    if (bCalibrating == HIGH) {
      return; // consecutive fails are normal during calibration. So just ignore and return.
    }

    else {
      bitWrite(statusReg, p, HIGH);
      bitWrite(statusReg, TOFissue, HIGH);
      digitalWrite(pINT_module, LOW); // assert modules INT pin

      Serial.print(timeoutCountLimit);  Serial.println(" consecutive measurement timeouts have occured!!!");
      resetMAX(HIGH);
      delay(100);
      resetMAX(LOW);
      for (uint8_t p = 0; p < SC_TOF; p++)
        timeoutCounter[p] = 0; // reset all the counters
      return;
    }
  }
}


double correctTOF(double TOFdiff) {  // attempts to correct wavelength skips
  if (bErrorCorrect == LOW)
    return TOFdiff; // return uncorrected value

  if (abs(TOFdiff) < TOFlimit) {
    return TOFdiff;  // return uncorrected value
  }
  for (int i = 0; i >= -3; i--) {
    if ( abs(TOFdiff + (i * 1000)) < TOFlimit ) { // if adjusted value is between -1000 and 1000 ns
      TOFdiff = TOFdiff + (i * 1000);
#ifdef TOFDeepDebug
      Serial.print(" corrected to ");  Serial.print(TOFdiff);
#endif
      return TOFdiff;  // return adjusted value
    }
  }
  return TOFdiff; // return uncorrected value
}


// SIMULATE TIME OF FLIGHT MEASUREMENT
#ifdef simulation_mode
void simulateMAX35(uint8_t p, double *dn, double *up, double *diff) {
#ifdef TOFDeepDebug
  Serial.println();  Serial.print("   Simulating TOF, channel "); Serial.print(p + 1);
#endif
double simulatedTOF[2] = {0};

if (batch == 1) {
  #ifdef TOFDeepDebug
  Serial.println(".  pumps BATCHING");
  #endif
  // add in a no-response timeout exception??

  simulatedTOF[0] = 1000 * expectedTOF + (sTOF[p] / 2) + random(-sVariance[p], sVariance[p]); // going upstream takes longer
  simulatedTOF[1] = 1000 * expectedTOF - (sTOF[p] / 2) + random(-sVariance[p], sVariance[p]);
  for (uint8_t i = 0; i < 2; i++) {
    if (random(0, 100) < (sSkipChance[p] / 2)) {
      simulatedTOF[i] += 1000 * random(-8, 8);  // arbitrary +/- 8 microsec range of skips
    }
    if (random(0, 100) < (sFails[p] / 2)) {
      simulatedTOF[i] = 9999999;
    }
  }

  *up = simulatedTOF[0];
  *dn = simulatedTOF[1];
  *diff = *up - *dn;
}
else {
  #ifdef TOFDeepDebug
  Serial.println(".  pumps IDLE");
  #endif
  simulatedTOF[0] = 1000 * expectedTOF + (1 / 2) + random(-sVariance[p]/5, sVariance[p]/5); // going upstream takes longer
  simulatedTOF[1] = 1000 * expectedTOF - (1 / 2) + random(-sVariance[p]/5, sVariance[p]/5);
  for (uint8_t i = 0; i < 2; i++) {
    if (random(0, 100) < (sSkipChance[p] / 2)) {
      simulatedTOF[i] += 1000 * random(-8, 8);  // arbitrary +/- 8 microsec range of skips
    }
    if (random(0, 100) < (sFails[p] / 2)) {
      simulatedTOF[i] = 9999999;
    }
  }

  *up = simulatedTOF[0];
  *dn = simulatedTOF[1];
  *diff = *up - *dn;
}

// flip batch state if start time + duration is now less than current time.
if (batchTimer + (batchDuration*60000l) < millis()) {
  batch = !batch;
  batchTimer = millis();
}

}
#endif


void printTOF() {
  double UPave = 0;
  double DNave = 0;
  Serial.println();  Serial.println();

  // MEASUREMENT COUNT AND SUCCESS RATIO
  Serial.print(" Measurement ");  Serial.print(dataDump_counter); Serial.print(".  Per-channel success rates: ");
  for (int p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.
    Serial.print(TOF[p].successRatio);  Serial.print("%,  ");
  }
  Serial.println();

  // AVERAGE UP AND DN TIMES, AND THEIR SD
  for (int p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.
    UPave = 0;
    DNave = 0;
    for (uint8_t i = 0; i < m; i++) {
      UPave = UPave + (double)(TOF[p].UP[i] / m);
      DNave = DNave + (double)(TOF[p].DN[i] / m);
    }
    Serial.print("CH");  Serial.print(p + 1);
    Serial.print(".  AVE_UP: ");  Serial.print(UPave / 1000000, 3);    Serial.print(" us.   UP_SD ");  Serial.print(TOF[p].UP_SD / 1000);  Serial.print(" ns.   ");
    Serial.print("AVE_DN: ");  Serial.print(DNave / 1000000, 3);    Serial.print(" us.   DN_SD ");  Serial.print(TOF[p].DN_SD / 1000);  Serial.print(" ns.   ");
    Serial.print("Diff_SD: ");  Serial.print((double)TOF[p].diffSD / 1000, 1);  Serial.print(" ns.   ");
    Serial.print("A_Diff: ");  Serial.print(((double) TOF[p].diff + TOF[p].baseline) / 1000, 1);  Serial.print(" ns.   ");
    Serial.print("H_Diff: ");  Serial.print(((double) TOF[p].diff + TOF[p].baseline) / 1000, 1);  Serial.println(" ns.   ");
  }
  // time difference and flow
  for (int p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.
    Serial.print("CH");  Serial.print(p + 1);
    Serial.print(".   AB_Diff: ");  Serial.print(((double)TOF[p].diff / 1000), 1);  Serial.print(" ns.   ");
    Serial.print("HB_Diff: ");  Serial.print(((double)TOF[p].diff / 1000), 1);  Serial.print(" ns.   ");
    Serial.print("Vel: ");  Serial.print(TOF[p].flowVel_ms, 5);  Serial.print(" m/s.   ");
    Serial.print("Vol: ");  Serial.print(TOF[p].flowVol_ls, 5);  Serial.print(" l/s.   ");
  }
  Serial.print("Corrected combined flow is ");  Serial.print((float)combinedFlow / 1000000);  Serial.println(" l/s");

}




// THIS FUNCTION IS NOT USED CURRENTLY
//void maxTOFerror(uint8_t p) {
//  if ( (bBaselineTOF == LOW) && (bCalibrateTOF == LOW) ) { // don't clear any results during a baseline/calibration
//    if ( TOF[p].successRatio < 30 ) {  // if success ratio was terrible then clear the registers
//      Serial.print("       Success ratio only ");  Serial.println(TOF[p].successRatio);
//      Serial.println(" !! Clearing results registers !!");
//
//      clearResults(p); // clearing all the results registers
//      bitWrite(statusReg, TOFissue, HIGH);
//      digitalWrite(pINT_module, LOW); // assert modules INT pin
//    }
//  }
//}
