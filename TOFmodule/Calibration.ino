//************************* BASELINE ALL MAX35 TOF PAIRS *************************
// Runs a very long set of measurements (assuming a zero flow) and then applies this fixed time baseline to each transducer pair
// Future update should include ability to calibrate at an arbitrary flow rate
boolean baselineTOF() {
  bCalibrating = HIGH; // to stop timeouts triggering in measureTOF
#ifdef GENDebug
  Serial.println();  Serial.println("Baselining... ");
#endif

  WDT_timings_t config;
  config.trigger = 2; // seconds before timeout that callback function is triggered
  config.timeout = baselineTime + dogTimeout; // allow enough time for baseline to complete.
  config.callback = myCallback;
  wdt.begin(config);

  for (int p = 0; p < SC_TOF; p++) { // clearing out old baseline. Don't apply anything to new baseline!
    TOF[p].baseline = 0;
  }

  boolean issue = 0; // goes high if there's an issue
  boolean bErrorCorrectBackup = bErrorCorrect;
  //bErrorCorrect = HIGH; // do we try and correct for errors? CURRENTLY MASTER SETTING DECICES THIS

  uint16_t aveTimeBackup = aveTimeTOF;
  aveTimeTOF = baselineTime * 1000; // temporarily change (increase) average size (baseline in sec, avetime in ms)

  measureTOF();

  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.

    //TODO check quality score etc and see if baseline is acceptable.
    if (TOF[p].successRatio >= 3 * cha_cutoff) {
      TOF[p].baseline = TOF[p].diff;
#ifdef GENDebug
      Serial.print("Channel ");  Serial.print(p + 1);  Serial.print(" - ");  Serial.print(baselineTime); Serial.print(" second baseline yielded asymmetric offset of ");
      Serial.print((double)TOF[p].baseline / 1000, 3); Serial.println("ns");
#endif
    }
    else {
#ifdef GENDebug
      Serial.println();  Serial.println();
      Serial.print("!!!! Channel ");  Serial.print(p + 1);  Serial.println(" BASELINE QUALITY IS LOW !!!!");
      Serial.print("SUCCESS percentage is ");  Serial.println(TOF[p].successRatio);
#endif
      bitWrite(statusReg, p, HIGH); // specify failed channel
      bitWrite(statusReg, BLcalIssue, HIGH); // issue calibrating or baselining
      digitalWrite(pINT_module, LOW); // assert int pin
      issue = 1;
    }
  }

  // restore master settings from backups
  aveTimeTOF = aveTimeBackup;
  bErrorCorrect = bErrorCorrectBackup;
  config.timeout = dogTimeout; // resetting watchdog timeout to usual value


#ifdef GENDebug
  Serial.println("Baselining complete.");
#endif
  return issue; // success is 0, issue is 1
}//***************************** END BASELINE TOF *****************************


void calibrate_comparators() {

  WDT_timings_t config;
  config.trigger = 2; // seconds before timeout that callback function is triggered
  config.timeout = compCalTime * (127 / res) + dogTimeout; // allow enough time for baseline to complete.
  config.callback = myCallback;
  wdt.begin(config);
  bCalibrating = HIGH;
  boolean  bErrorCorrectBackup = bErrorCorrect;  // remember master setting to restore later
  uint16_t aveTimeTOF_Backup   = aveTimeTOF;     // remember original setting
  //boolean  USTenable_Backup[SC_TOF] = {0};


  // STEP 2, HIGH AVE SIZE. , SUCCESS RATIO, SD AND ZERONESS QUERIED. TEST TREATS UPSTREAM AND DOWNSTREAM TOGETHER.
  // Gives all shortlist a score
  bErrorCorrect = LOW; // DO NOT correct during calibration. Ideally we are finding a setting which will v rarely need correction.
  aveTimeTOF = 1000 * compCalTime; // convert seconds into ms


  for (uint8_t p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers
#ifdef GENDebug // print out which channels are involved
    else {
      Serial.print("Channel ");  Serial.print(p + 1);  Serial.print(" is active. ");
    }
#endif
    TOF[p].comparatorOffset_det[0] = 0;  // start testing at 0 offset
    TOF[p].comparatorOffset_det[1] = 0;
  }

  uint8_t testCount = 0;
  uint8_t trialOffset_det = 0;
  boolean channelOKAY[SC_TOF] = {LOW};

  while (trialOffset_det <= 127) { // MAIN MEASUREMENT AND SCORE LOOP
#ifdef GENDebug
    Serial.println();  Serial.println();  Serial.println();
    Serial.print("Testing signal offset of ");  Serial.print(trialOffset_det); Serial.print(". TestCount is ");  Serial.print(testCount);  Serial.println("... ");
#endif
    wdt.feed(); // ensure watchdog isn't triggered during longer calibrations
    measureAndScore(testCount);
    trialOffset_det = trialOffset_det + res; // increment tested voltage

    for (uint8_t p = 0; p < SC_TOF ; p++) {
      if (USTenable[p] == LOW) continue; // ignore any disabled transducers
      if (TOF[p].successRatio >= cha_cutoff) // a single decent (> cutoff percent) measurement means the channel is probably okay. don't disable it.
        channelOKAY[p] = HIGH;

      TOF[p].comparatorOffset_det[0] = trialOffset_det;
      TOF[p].comparatorOffset_det[1] = trialOffset_det;
    }
    testCount++;
  }
  wdt.feed(); // ensure watchdog isn't triggered during longer calibrations
#ifdef GENDebug
  Serial.println("Calibration complete.");
#endif


  for (uint8_t p = 0; p < SC_TOF ; p++) {  // disabling any channels which totally failed
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers
    if (channelOKAY == LOW) {
      USTenable[p] = LOW;
#ifdef ErrorDebug
      Serial.println(); Serial.print("Channel "); Serial.print(p + 1);
      Serial.println("!! Every calibration setting attempted failed to perform. !!");
      Serial.println("!!         DISABLING ULTRASONIC TOF SENSOR CHANNEL        !!");
#endif
      bitWrite(statusReg, p, HIGH); // specify failed channel
      bitWrite(statusReg, BLcalIssue, HIGH); // issue calibrating or baselining
      // not asserting INT pin because that happens at end of function.
    }
  }


  // STEP 3. SELECT THE BEST VALUE FROM SHORTLIST
  float score = 0;
  float bestScore[SC_TOF] = {0};
  uint8_t bestScore_index[SC_TOF] = {0};

  for (uint8_t p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers

#ifdef GENDebug // printing progress
    for (int i = 0; i < testCount; i++) {
      Serial.print("voltage offset of "); Serial.print(i * res);  Serial.print(" scored ");  Serial.println(total_score[p][i]);
    }
#endif

    for (int i = 2; i < (testCount - 2); i++) { // start at i=2 to ignore lowest most values, and end 2 early to ignore most extreme values
      // previous and subsequent scores also taken into account. If the best score is at a far extreme then the gain should be adjusted
      score = 0.1 * total_score[p][i - 2] + 0.2 * total_score[p][i - 2] + 0.4 * total_score[p][i] + 0.2 * total_score[p][i + 1] + 0.1 * total_score[p][i + 2];
      if (score > bestScore[p]) {
        bestScore[p] = score;
        bestScore_index[p] = i;
      }
    }

    TOF[p].comparatorOffset_det[0] = res * bestScore_index[p];
    TOF[p].comparatorOffset_det[1] = res * bestScore_index[p];
#ifdef GENDebug
    Serial.print("Channel ");  Serial.print(p + 1);
    Serial.print(" - Best aggregated score is ");  Serial.print(bestScore[p]);  Serial.print(" with offset level of ");  Serial.println(res * bestScore_index[p]);
    Serial.print("Voltage offset level of ");  Serial.print(TOF[p].comparatorOffset_det[0]);  Serial.println(" selected. ");  Serial.println();
#endif
  }

  generate_MAXconfig(); // upload to max35 ICs
  upload_MAXconfig(-1);

  // STEP 4. TWEAK EACH TRANSDUCER INDIVIDUALLY AT HIGH res
  uint8_t range = 10; // how far above and below the rough calibration is checked?
  uint8_t res = 2; //
  uint8_t bestOffset[SC_TOF][2] = {0};
  

  //for (int r = 1; r <= 2; r++ ) {        // multiple repeats of fine calibration
  for (int UST = 0; UST <= 1; UST++) {     // start at upstream (0), then do downstream (1)
    memset(bestScore, 0, sizeof(bestScore));   // reset this, as different metrics are now in use, and when going from UP to DN it needs clearing.
    Serial.println();
    Serial.println();
    if (UST == 0) Serial.println("*** Calibrating UPSTREAM transducers ***"); else Serial.println("*** Calibrating DOWNSTREAM transducers ***");
    for (int p = 0; p < SC_TOF; p++) {
      TOF[p].comparatorOffset_det[UST] -= (range+res); // subtract range, will be slowly added back during calibration
    }

    for (int i = 0; i < range * 2; i += res) {     // trying different comparator offsets
      Serial.println(); Serial.println("Starting next test...");
      for (int p = 0; p < SC_TOF; p++) {
        if (USTenable[p] == LOW) continue; // ignore any disabled transducers
        if(TOF[p].comparatorOffset_det[UST] + res < 127) {    // don't increment if it would overflow   
          TOF[p].comparatorOffset_det[UST] += res;  // increment offset by res for each channel
        }
        Serial.print("Cha ");  Serial.print(p+1);  Serial.print(", testing off set ");  Serial.println(TOF[p].comparatorOffset_det[UST]);
      }

      measureAndScore_hiRes(i, UST);
      for (int p = 0; p < SC_TOF; p++) {
        if (USTenable[p] == LOW) continue; // ignore any disabled transducers
        if (total_score[p][i] > bestScore[p]) {
          bestScore[p] = total_score[p][i];
          bestOffset[p][UST] = TOF[p].comparatorOffset_det[UST];
          Serial.print("Cha ");  Serial.print(p+1);  Serial.print(", new best score is ");  Serial.print(bestScore[p]);  Serial.print(" from offset of ");  Serial.println(bestOffset[p][UST]);
        }
      }
    }
    for (int p = 0; p < SC_TOF; p++) {
      if (USTenable[p] == LOW) continue; // ignore any disabled transducers
      TOF[p].comparatorOffset_det[UST] = bestOffset[p][UST];
    }
    #ifdef GENDebug
  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers
    Serial.println();  Serial.println();
    Serial.print("Channel ");  Serial.println(p + 1);
    if (UST ==0) {
    Serial.print("UPSTREAM best score is ");  Serial.print(bestScore[p]);  Serial.print(" with offset of ");  Serial.println(bestOffset[p][0]);
    Serial.print("UPSTREAM voltage offset level of ");  Serial.print(TOF[p].comparatorOffset_det[0]);  Serial.println(" selected. ");
    }
    else {
    Serial.print("DOWNSTREAM best score is ");  Serial.print(bestScore[p]);  Serial.print(" with offset of ");  Serial.println(bestOffset[p][1]);
    Serial.print("DOWNSTREAM voltage offset level of ");  Serial.print(TOF[p].comparatorOffset_det[1]);  Serial.println(" selected. ");
    }
  }
#endif
  }
  // }

  generate_MAXconfig(); // ensure everything is uploaded.
  upload_MAXconfig(-1);
  

  aveTimeTOF = aveTimeTOF_Backup;         // revert to original settings
  bErrorCorrect = bErrorCorrectBackup;
  bCalibrating = LOW;

#ifdef GENDebug
  Serial.println("Calibration complete.");
#endif


  config.timeout = dogTimeout; // revert to original watchdog timeout duration
  bCalibrating = LOW; // finished calibrating..

}//***************************** END OF CALIBRATE COMPARATORS *****************************


// UPDN = 0 for UPSTEREAM, and 1 for DOWNSTREAM
//float measureAndScore_hiRes(uint8_t i, boolean UPDN) {
void measureAndScore_hiRes(uint8_t i, boolean UPDN) {
  float   SDdiff_score[SC_TOF]    = {0};
  float   SD_score[SC_TOF]    = {0};
  uint8_t ratio_score[SC_TOF] = {0};


  generate_MAXconfig();
  upload_MAXconfig(-1);
  measureTOF();

  for (uint8_t p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers

    ratio_score[p]  = 1000 / (110 - (TOF[p].successRatio));

    SDdiff_score[p] = 2000 / (float)(TOF[p].diffSD / 100);
    if (TOF[p].diffSD == 0)  // if SD is 0 then probably error
      SDdiff_score[p] = 0;


    if (UPDN == 0) { // UPSTREAM
      SD_score[p]     = 2000 / (float)(TOF[p].UP_SD / 100);
      if (TOF[p].UP_SD == 0)  // if SD exactly 0, then error so score = 0.
        SD_score[p] = 0;
    }
    else {          // DOWNSTREAM
      SD_score[p]     = 2000 / (float)(TOF[p].DN_SD / 100);
      if (TOF[p].DN_SD == 0)  // if SD exactly 0, then error so score = 0.
        SD_score[p] = 0;
    }

    total_score[p][i] = (SDdiff_score[p]  + 4 * SDdiff_score[p] + 3 * ratio_score[p]) / 8;
  }


#ifdef GENDebug // printing results
  Serial.println();
  for (uint8_t p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers
    
    Serial.print("Cha ");  Serial.print(p+1);  Serial.print(", offset = "); 
    
    if (UPDN == 0) {
      Serial.print(TOF[p].comparatorOffset_det[0]);
      Serial.print(". SD UPSTREAM = ");  Serial.print((float)TOF[p].UP_SD / 1000, 1);       Serial.print(" ns. SD_UP score = ");  Serial.print(SD_score[p]);
    }
    else {
      Serial.print(TOF[p].comparatorOffset_det[1]);
      Serial.print(". SD DOWNSTREAM = ");  Serial.print((float)TOF[p].DN_SD / 1000, 1);       Serial.print(" ns. SD_DN score = ");  Serial.print(SD_score[p]);
    }
    Serial.print("    SD_diff = ");  Serial.print((float)TOF[p].diffSD / 1000, 3);       Serial.print(" ns.   SD_score = ");  Serial.print(SDdiff_score[p]);
    Serial.print("    Ratio = ");  Serial.print(TOF[p].successRatio);  Serial.print(" %. Ratio_score = ");  Serial.print(ratio_score[p]);
    Serial.print("    Total_score = ");  Serial.println(total_score[p][i]);
  }
#endif
}

//float measureAndScore(uint8_t i) {
void measureAndScore(uint8_t i) {
  float   SDdiff_score[SC_TOF]    = {0};
  float   zero_score[SC_TOF]    = {0};
  float ratio_score[SC_TOF] = {0};
  //float   total_score[SC_TOF] = {0};

  generate_MAXconfig();
  upload_MAXconfig(-1);
  measureTOF();

  for (uint8_t p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers

    SDdiff_score[p]    = 2000 / (float)(TOF[p].diffSD / 100);
    //SD_score[p]    = 2000 / (float)(TOF[p].diffSD / 100);
    ratio_score[p] = 1000 / (110 - (TOF[p].successRatio));

    // TOF and SD will return 0 if every measurement is rejected due to extreme inaccuracy. account for this below.
    if (TOF[p].diffSD == 0)  // if SD is crazy small then probably error
      SDdiff_score[p] = 0;
    if (TOF[p].diff == 0)
      zero_score[p] = 0;

    total_score[p][i] = (SDdiff_score[p] * 2 + zero_score[p] + ratio_score[p] * 4 ) / 7;
  }
#ifdef GENDebug // printing results
  Serial.println();
  Serial.print("Signal offset of ");  Serial.println(TOF[0].comparatorOffset_det[0]);
  for (uint8_t p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers
    Serial.print("SD = ");  Serial.print((float)TOF[p].diffSD / 1000, 3);       Serial.print(" ns.   SD_score = ");  Serial.print(SDdiff_score[p]);
    Serial.print("    TOF = ");  Serial.print((float)TOF[p].diff / 1000, 1);       Serial.print(" ns. Zero_score = ");  Serial.print(zero_score[p]);
    Serial.print("    Ratio = ");  Serial.print(TOF[p].successRatio);  Serial.print(" %. Ratio_score = ");  Serial.print(ratio_score[p]);
    Serial.print("    Total_score = ");  Serial.println(total_score[p][i]);
  }
#endif
}


// applied by EEPROM_initialisation if every memory block fails it's CRC check, indicating corruption.
void defaultCalibration() {
#ifdef GENDebug
  Serial.print("Applying default baseline and comparator calibration values...");
#endif

  // DEFAULT BASELINE
  TOF[0].baseline  = DEFbaseline1;    // this baseline (ps) is SUBTRACTED from all TOFdifferences AVERAGES.
  TOF[1].baseline  = DEFbaseline2;    // this baseline (ps) is SUBTRACTED from all TOFdifferences AVERAGES.
  TOF[2].baseline  = DEFbaseline3;    // this baseline (ps) is SUBTRACTED from all TOFdifferences AVERAGES.
  TOF[3].baseline  = DEFbaseline4;    // this baseline (ps) is SUBTRACTED from all TOFdifferences AVERAGES.

  // DEFAULT CALIBRATION
  TOF[0].comparatorOffset_det[0] = DEFcomOffsetD1UP;
  TOF[0].comparatorOffset_det[1] = DEFcomOffsetD1DN;
  TOF[1].comparatorOffset_det[0] = DEFcomOffsetD2UP;
  TOF[1].comparatorOffset_det[1] = DEFcomOffsetD2DN;
  TOF[2].comparatorOffset_det[0] = DEFcomOffsetD3UP;
  TOF[2].comparatorOffset_det[1] = DEFcomOffsetD3DN;
  TOF[3].comparatorOffset_det[0] = DEFcomOffsetD4UP;
  TOF[3].comparatorOffset_det[1] = DEFcomOffsetD4DN;

  TOF[0].comparatorOffset_ret[0] = DEFcomOffsetR1UP;
  TOF[0].comparatorOffset_ret[1] = DEFcomOffsetR1DN;
  TOF[1].comparatorOffset_ret[0] = DEFcomOffsetR2UP;
  TOF[1].comparatorOffset_ret[1] = DEFcomOffsetR2DN;
  TOF[2].comparatorOffset_ret[0] = DEFcomOffsetR3UP;
  TOF[2].comparatorOffset_ret[1] = DEFcomOffsetR3DN;
  TOF[3].comparatorOffset_ret[0] = DEFcomOffsetR4UP;
  TOF[3].comparatorOffset_ret[1] = DEFcomOffsetR4DN;

  generate_MAXconfig();
  upload_MAXconfig(-1);
  Serial.println();
}


void calibrationSummary() {
#ifdef GENDebug
  Serial.println();  Serial.println();
  Serial.println("Comparator detection offset voltages:");
  for (int p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.
    Serial.print("Channel "); Serial.print(p + 1);
    Serial.print(" - Upstream Transducer ");  Serial.print(TOF[p].comparatorOffset_det[0]);  Serial.print("  Downstream Transducer ");  Serial.println(TOF[p].comparatorOffset_det[1]);
  }
  Serial.println();
  Serial.println("Comparator return offset voltages:");
  for (int p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.
    Serial.print("Channel "); Serial.print(p + 1);
    Serial.print(" - Upstream Transducer ");  Serial.print(TOF[p].comparatorOffset_ret[0]);  Serial.print("  Downstream Transducer ");  Serial.println(TOF[p].comparatorOffset_ret[1]);
  }
  Serial.println();
  for (int p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.
    Serial.print("Channel "); Serial.print(p + 1);
    Serial.print(" - Baseline ");  Serial.println((float)TOF[p].baseline / 1000, 2);
  }

  Serial.println();
#endif
}
