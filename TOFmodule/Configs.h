#ifndef _CONFIGS_H    // Put these two lines at the top of your file.
#define _CONFIGS_H

//********* FIXED SETTINGS ***********
uint32_t serialNum_module = 12345;    // module serial number
#define software_version 1.04         // updates to grab more metrics data from MJL
#define I2C_Address 22                // address for I2C comms to controller. 
#define SC_TOF  4                     // Configuration of the PCB. How many MAX35104 chips present?
#define fluid_count  4                // how many fluid types are in the system?
#define flow_logSize 10            // how many flow measurements are used to make the running average?
#define pulseVoltageMax 12             // maximum voltage level for ATEX
//**************************************

//***** DEBUG OPTIONS**********
#define errorDebug 1        // all critical errors  
#define coreDebug 1         // only most crtical flow information 
#define GENDebug 1          // general debug messages, including calibration
//#define INTDebug 1        // are interrupts from MAX35 chips printed to serial?
//#define TOFDebug 1        // all flow debugs except...
#define TOFDeepDebug 1    // raw time data
//#define I2CDebug 1        // most prints concerning I2C
//#define I2CDeepDebug 1    // print individual bytes for I2C
#define flowDebug 1         // concerns calculation and correction of flow readings
#define eepromDebug 1     // Important prints involving EEPROM
//#define eepromDeepDebug 1 // individual cell and block values, etc etc... 

//********************************

//********** USER SETTINGS **********
#define simulation_mode    1             // if defined then flow measurements will be simulated and not actually performed.
#define dogTimeout         30              // time in seconds before watchdog times out
boolean USTenable[SC_TOF] = {1, 1, 1, 1};  // This status register is used to disable transducers.
boolean bSerialControl    = 1;             // when debugging mode is on serial control may optionally be enabled
boolean correctFlow       = 1;             // when HIGH correction factors will be applied based on the reynolds number.
boolean bErrorCorrect     = 0;             // if HIGH then code will attempt to correct for TOF wavelength skips
uint8_t aveMethod         = 0;             // 0 is arithmetic mean, 1 is harmonic mean
//**********************************

// SIMULATION MODE SETTINGS
#ifdef simulation_mode
//boolean batchMode      = 1;                     // toggles batch simulation. 1 is ON. //not used!!!!
uint16_t sTOF[4]       = {195, 198, 200, 202};  // tof difference in ns
uint16_t sVariance[4]  = {5, 5, 10, 10};        // range on randomness applied
uint8_t sSkipChance[4] = {5, 5, 15, 5};          // proportion of measurements which skip a wavelength
uint8_t sFails[4]      = {5, 5, 5, 15};         // percentage of measurements which totally fail
uint32_t batchDuration  = 2;                     // batch time in minutes. half of total time is spent in batches.
uint32_t batchTimer    = millis();              // just a timer for batches.
boolean batch          = 1;                     // when 1, a batch is currently being simulated
#endif

// CONFIG DEFAULTS AND LIMITS

// CALIBRATION ********************
#define LBbaselineTime    1
#define DEFbaselineTime   30 // How many seconds for baseline?
#define HBbaselineTime    255

#define LBcompCalTime     1
#define DEFcompCalTime    3 // how many seconds for each measurement trial PER comparator setting for calibration?
#define HBcompCalTime     255

#define  LBres            1
#define  DEFres           6              // Step resolution for comparator calibration. ideally a multiple of 128, or close.
#define  HBres            16

#define DEFbaseline1      100
#define DEFbaseline2      200
#define DEFbaseline3      300
#define DEFbaseline4      400

#define DEFcomOffsetD1UP    105
#define DEFcomOffsetD1DN    105
#define DEFcomOffsetD2UP    63
#define DEFcomOffsetD2DN    63
#define DEFcomOffsetD3UP    93
#define DEFcomOffsetD3DN    93
#define DEFcomOffsetD4UP    69
#define DEFcomOffsetD4DN    69

#define DEFcomOffsetR1UP    0
#define DEFcomOffsetR1DN    0
#define DEFcomOffsetR2UP    0
#define DEFcomOffsetR2DN    0
#define DEFcomOffsetR3UP    0
#define DEFcomOffsetR3DN    0
#define DEFcomOffsetR4UP    0
#define DEFcomOffsetR4DN    0
//*********************************

// TIME-OF-FLIGHT CONFIGURATION ***
#define LBaveTimeTOF 500
#define DEFaveTimeTOF 2700
#define HBaveTimeTOF (dogTimeout-1)*1000 // measurements must be 1 second faster than system timeout

#define LBrising_falling 0
#define DEFrising_falling 1
#define HBrising_falling 1

#define LBpulseVoltage  1
#define DEFpulseVoltage 12
#define HBpulseVoltage  pulseVoltageMax

#define LBlaunch_delay  70
#define DEFlaunch_delay 1370  // in microseconds
#define HBlaunch_delay  10000

#define LBtimeoutLimit 5
#define DEFtimeoutLimit 30
#define HBtimeoutLimit 500
//*********************************

// DATA PROCESSING CONFIG
#define LBaveMethod 0
#define DEFaveMethod 1
#define HBaveMethod 1

#define LBexpectedTOF 50
#define DEFexpectedTOF 445    // in microseconds. 333 for 8" 414 for 10". 445 10" penspen
#define HBexpectedTOF 4000

#define LBrxWindow 10
#define DEFrxWindow 50     // divided by 5 and +/- to expected TOF to provide accepted window on UP and DN time
#define HBrxWindow 500

#define LBTOFlimit 10
#define DEFTOFlimit 400    // for Penspen we will use 400.
#define HBTOFlimit 10000

#define LBSD_threshold 1
#define DEFSD_threshold 16
#define HBSD_threshold 30

#define LBcha_cutoff 0
#define DEFcha_cutoff 15
#define HBcha_cutoff 95

#define LBcorrectFlow 0
#define DEFcorrectFlow 1
#define HBcorrectFlow 1

#define LBbErrorCorrect 0
#define DEFbErrorCorrect 1
#define HBbErrorCorrect 1
//***********************************

// PIPE AND INSTALLATION
#define LBtemperature -1000
#define DEFtemperature 2000
#define HBtemperature 5000

#define LBfluid 0
#define DEFfluid 1 // kerosene
#define HBfluid 3

#define LBtAngle 50
#define DEFtAngle 400
#define HBtAngle 450

#define LBSoSt  100000
#define DEFSoSt 2879000
#define HBSoSt  6000000

#define LBbounceCount 0
#define DEFbounceCount 1
#define HBbounceCount 5

#define LBpDi 100
#define DEFpDi 203  // 254 = 10", 203 = 8"
#define HBpDi 400
//********************************


// TIME-OF-FLIGHT CONFIGURATION
uint8_t  pulseVoltage   = DEFpulseVoltage;              // Target output voltage leFlowVel_ms. starts at 0100. 1=9, 2=10.8, 3=12.6, 4=15, 5=16.8, 6=19.2, 7=21, 8=22.8, 9=25.2, 10=27, 11=28.8, 12=30.6V
uint16_t aveTimeTOF     = DEFaveTimeTOF;           // how many milliseconds to allow for a full measurement?
uint8_t  baselineTime    = DEFbaselineTime;             // How many seconds for baseline?
uint8_t  compCalTime     = DEFcompCalTime;              // how many seconds for each measurement trial PER comparator setting for calibration?
uint8_t  res            = DEFres;              // Step resolution for comparator calibration. ideally a multiple of 128, or close.
uint8_t  timeoutLimit   = DEFtimeoutLimit;             // time in milliseconds before a timeout is triggered  (on single diff measurement)
uint8_t  cha_cutoff     = DEFcha_cutoff;             // percentage of success ratio below which the channel is ignored / disabled. Baseline requires tripple this to pass
uint16_t launch_delay   = DEFlaunch_delay;            // delay between starting subsequent measurements in us
uint16_t expectedTOF    = DEFexpectedTOF;            // in microseconds
uint8_t  rxWindow       = DEFrxWindow;             // time in microseconds before and after expected TOF in which reciever is active
boolean  rising_falling = DEFrising_falling;              // 0 is rising edge, 1 is falling edge.
uint16_t TOFlimit       = DEFTOFlimit;            // The maximum (abs) TOF difference in ns which is acceptable.
uint8_t  SD_threshold   = DEFSD_threshold;             // how many tenths of a standard deviation before data is outlier?
uint8_t  pulseLength    = 18;             // how many wavelengths in a pulse. Careful changing this, as the hit-waves are currently fixed and depend on this.
boolean  single_ended   = 0;             // When 0, drive is differential.
//*****************************************


// PIPE AND TRANSDUCER CONFIG
uint32_t SoSt           = 2879000;       // Transducer (longitudinal) speed of sound in mm/s
uint16_t tAngle         = 400;           // transducer angle in tenths of a degree (API needs update)
uint16_t pDi            = DEFpDi;           // pipe diameter in mm
uint8_t  bounceCount    = 1;
uint8_t  fluid          = 1;             // 0 is water, 1 is kerosene, 2 is aztec, 3 is methane, 255 is custom.
//*******************************************


// TOF DATA
int32_t combinedFlow = 0; // holds the combined running weighted average flow in ul/s
float   def_weights[flow_logSize] = {0.25, 0.14, 0.12, 0.1, 0.09, 0.08, 0.07, 0.06, 0.05, 0.04};  // default weighting when calc running average
int32_t flowprev = 0; //previous reported flow value
int32_t massFlow = 0; // mass flow calculated from flow
typedef struct  {
  uint16_t MAXconfig[13] = {0};     // holds the MAX35 register configurations

  uint32_t UP[256]                 = {4294967295}; // the size of these arrays is also the upper limit of aveSizeTOF
  uint32_t DN[256]                 = {4294967295};
  uint8_t  rawIndex[2]             = {0}; // index of raw UP/DN times to send on each opcode 1-8?
  uint32_t UP_SD                   = 0;
  uint32_t DN_SD                   = 0;
  int32_t  diffLogF[5000]          = {0}; // holds all TOFdiff values that made it through correction, filter, and SD removal.
  int32_t  diff                    = 0;  // [0] is arithmetic mean. All in ps.
  int32_t  diffSD                  = 0;
  uint8_t  successRatio            = 0;   // ratio of i (successful measurements) to total measure attempts (100 means every measurement succeeded).

  int32_t  baseline                = 0;    // this baseline (ps) is SUBTRACTED from all TOFdifferences AVERAGES.
  uint8_t  comparatorOffset_det[2] = {0}; // baseline applied to MAX35 internal compatator. Between 0 and 127. [0] is UP [1] is DN
  int8_t   comparatorOffset_ret[2] = {0};   // baseline applied to MAX35 internal compatator. Between 0 and 127. [0] is UP [1] is DN

  float    flowVel_ms              = 0;  // flow velocity in meters per second. Most recent mesurement.
  int32_t  flowVel_ums             = 0;  // flow velocity in micro meters per second, from arithmetic tof mean. Most recent mesurement.
  float    flowVol_ls              = 0;  // flow volume in litres per second. Most recent mesurement.
  int32_t  flowVol_uls             = 0;  // flow volume in micro litres per second. Most recent mesurement.
  int32_t  flowMass_gs             = 0;  // flow mass in grams per second. Most recent mesurement.

  int32_t  flowLog[flow_logSize]    = {0}; // holds the previous x combined flow measurements. index 0 is most recent
  float    qualityLog[flow_logSize] = {0};  // used to adjust weights below
  float    totalQuality             = 0;   // quality of the entire running average
  float    weights[flow_logSize]    = {0}; // holds scaled weighting
  int32_t  aveFlow_uls              = 0;   // per channel weighted running average flow (uL/s)
  boolean  agreement[4]             = {0}; // bits go high when agreeing with other channels (bit 0 channel 0 etc)
  uint8_t  agreeCount               = {0}; // how many other channels does this one agree with?
  boolean  accepted                 = 1; // if HIGH, the channel is included in combined flow calc. Default is include all

  uint64_t aveUPraw         = 0; // average of all raw UP times
  uint64_t aveDNraw         = 0; // average of all raw DN times
  uint64_t aveUPwinF        = 0; // average of times after window filter
  uint64_t aveDNwinF        = 0; // average of times after window filter
  uint32_t UPfilterLog[256] = {0}; // logs all times that pass window filter to calc aveUPallF
  uint32_t DNfilterLog[256] = {0}; // logs all times that pass window filter to calc aveUPallF
  uint64_t aveUPallF        = 0; // average of all filters
  uint64_t aveDNallF        = 0; // average of all filters

  uint32_t RoC1           = 0;
  uint32_t RoC2           = 0;
  
  uint32_t totalMeasureC  = 0;
  uint32_t timeoutC       = 0;  
  uint32_t filtMeasureC   = 0;
  uint32_t goodMeasureC   = 0;
  
} TOF_type;
TOF_type TOF[SC_TOF];
//************************************************************************************************************


// ACCUMULATED FLOW VARIABLES
uint16_t accVolAddress = 0; // holds the most recently written to EEPROM address
uint32_t accTime = 0; // time in milliseconds since the accumulated flow was last updated
uint32_t accTimeStart = 0; // epoc time when accumulated flow started.
boolean accVolContinue = HIGH; // used to account for downtime.
uint32_t downTime = 0; // seconds since last acc vol update to EEPROM.
boolean calibrationOK = LOW; // goes HIGH if a single CRC check passes during initialisation, indicating that the saved calibration is uncorrupted.

union accFlowUnion {
  double   volDouble;
  uint8_t  volBytes[8];
};
accFlowUnion accFlowU;

union accTimeUnion {
  uint32_t timeInt;
  uint8_t  timeBytes[4];
};
accTimeUnion accTimeU;

union baselineUnion {
  int32_t baselineInt;
  uint8_t  baselineBytes[4];
};
baselineUnion baselineU;


// PIN DEFINITIONS
// pin 11 is MOSI, pin 12 is MISO, pin 13 is SCK
const uint8_t pCS_MAX[4]  = {1, 2, 3, 4};     // chip select pins for MAX35 chips
const uint8_t pINT_MAX[4] = {6, 7, 8, 9};     // INT bar interupt pin for MAX35 chips.
const uint8_t pAF_EN[4]   = {14, 15, 16, 17}; // Enable for amplifier/filters. Active HIGH (1.7V threshold)
const uint8_t pSCL        = 19;               // arduino default I2C pin
const uint8_t pSDA        = 18;               // arduino default I2C pin
const uint8_t pINT_module = 20;               // interrupt pin for I2C comms with the TOF module

// GENERAL FLAGS
boolean  bTOFcompleteFlag[SC_TOF] = {LOW};  // goes HIGH when a ToF measurement has been completed. reset LOW when measurement is commanded.
boolean  bTOFtimeoutFlag[SC_TOF]  = {LOW};  // goes HIGH when a ToF measurement has timed out (from MAX35). reset LOW when measurement is commanded.
uint16_t intReg_MAX[SC_TOF]       = {0};    // MAX interupt registers
boolean  responded[SC_TOF]        = {0};    // goes HIGH when a transducer has responded during a simultaneous measurement
boolean  bIdleFlag                = HIGH;  // goes HIGH when the module re-enters the main loop so it knows to print the message
boolean  bCalibrating             = LOW;    // goes HIGH when calibrating comparators to signal fail timeouts mustn't be acted on

// TIME OF FLIGHT GLOBAL VARIABLES
uint16_t m = 0; // how many TOF measurements were attempted in previous flow measurement?
uint16_t timeoutCounter[SC_TOF]   = {0};       // keeps track of consecutive failed measurements
uint16_t timeoutCountLimit        = 10; // how many consecutive fails prompts a full MAX35 reset?
float    total_score[SC_TOF][128] = {0};  // holds the 'score' for each channel for each tested comparator offset
uint8_t  dataDump_counter         = 0;
uint8_t  activePairs              = 0;   // current number of enabled transducers

// FLOW CALCULATION VARIABLES
float    STP_density[fluid_count] = {998.2, 809.124074, 890.456586, 0};       // 0=water, 1=kerosene, 2=aztec, 3=methane. standard temp (25C) and pressure values Kg/m^3
float    STP_slope[fluid_count]   = { -0.3375, -0.746318, -0.627528, 0}; // Corrects density. 0=water, 1=kerosene, 2=aztec, 3=methane. density change per degree
float    STP_sos[fluid_count]     = {1480, 1324, 1400, 0};          // 0=water, 1=kerosene, 2=aztec, 3=methane. standard temp and pressure values m/s
int16_t  temperature              = DEFtemperature; // temperature of fluid in hundredths of degrees centigrade
double   density                  = 0;     // Kg/m3 resulting density from temp compensation
double   viscosity                = 0; // in Cst
double   SoSf                     = 0;
uint32_t reynolds_number          = 0;
int32_t  flow_scaler              = 1000; // Divided by 1000 and then the combined vol flow is multiplied by this
int32_t  flow_offset              = 0; // in micro litres per second. added to combined flow after scaler
uint8_t  flow_cutoff              = 3; // in litres per second
// CHANNEL FLOW COMBINATION VARIABLES


// I2C GLOBAL VARIABLES
union I2CUnion {
  uint32_t Uvalue;
  int32_t  Svalue;
  uint8_t  bytes[4];
};
I2CUnion I2Cunion; // create instance of union

SPISettings MAX_SPI(5000000, MSBFIRST, SPI_MODE1); // variable for SPI used for MAX35104. 5 MHz speed currently.
volatile uint8_t  opcode          = 0;   // Holds opcode commands sent via I2C
volatile uint8_t  I2C_instructionRecieved = 0;   // Holds the number of bytes recieved. byte 1 is always the opcode.
volatile uint8_t  bytesRecieved[4]  = {0}; // at most 4 bytes are recieved at a time
uint16_t statusReg               = 0;   // interrupt register for the TOF module
uint32_t I2Cdata                 = 0;   // I2C ISR loads data for sending into this variable
uint8_t  I2CtoSend[4]            = {0};
uint8_t  I2CbyteCount            = 0;   // I2C ISR loads number of bytes to send into this variable
float    I2Ccheck                = 0;   // holds the current value of the previously changed config
#define I2Cpanic         2863311530;    // loaded onto I2C buffer during panic. Equals 00001111,00001111,00001111,00001111

boolean  bStartTOF         = LOW;    // when HIGH a set of TOF measurements will be made on main loop
uint8_t  bSendArray = 0;             // 0 = no array request. 1 = upstream time, 2 = downstream time.



//************ STATUS REGISTER **********************************************
#define Cha1 0            // channel identifier
#define Cha2 1            // channel identifier 
#define Cha3 2            // channel identifier
#define Cha4 3            // channel identifier 
#define POR 4             // reset / power on completed
#define awaiting 5        // module is awaiting instructions
#define configSuccess  6  // config change success
#define configIssue 7     // config issue
#define BLcal 8           // baseline or calibration finished
#define BLcalIssue 9      // issue with baseline or calibration
#define TOFprog 10        // Flow measurement in progress
#define TOFdone 11        // Flow measurement completed
#define TOFissue 12       // Flow measurement issue
#define I2CdataLoaded  13 // Requested data ready to read
#define I2CdataError  14  // issue sending data
#define invalidOpcode  15 // default triggered on switch case / ifelse chain

//*****************************************************************************


// ****************** TURBULENCE CORRECTION FACTORS ******************
// {k, Re}  correction factor data vs reynolds number
#define Ktemps_count 3          // How many temperature tables are in the correction factor array?
#define DARREN_FIX 16
int16_t kTemps[Ktemps_count] = {-1000, 1700, 3000};  // defines the temperatures that each table of the below matrix holds
float k_data[Ktemps_count][DARREN_FIX][Ktemps_count] = { // 1st index is -10C (spoofed) 2nd is 17C, 3rd is 30C.
  { // -10C
    {0.82830, 0},
    {0.82830, 300}, 
    {0.80652, 600},  
    {0.78060, 1200},  
    {0.85079, 1800},
    {1.01587, 2400},
    {1.01440, 10000},
    {1.01145, 25000},
    {1.01210, 40000},
    {1.02017, 60000},
    {1.03189, 80000},
    {1.03493, 100000},
    {1.03908, 120000},
    {1.05372, 170000},
    {1.05492, 225000},
    {1.05492, 999999},
  },
  { // 17C
    {0.79456, 0},
    {0.79456, 1},  // padding to match array size
    {0.79456, 2},  // padding to match array size
    {0.79456, 3},  // padding to match array size
    {0.79456, 296},
    {0.77207, 610},
    {0.74365, 1275},
    {0.78607, 1783},
    {0.98299, 1892},
    {0.98214, 2324},
    {0.97709, 28416},
    {0.97451, 56890},
    {0.97401, 113688},
    {0.97712, 169593},
    {0.97777, 224364},
    {0.97777, 999999},
  },
  {  // 30C
    {0.74041, 0},
    {0.74041, 687},
    {0.71958, 1418},
    {0.94340, 2158},
    {0.95042, 3193},
    {0.96139, 4281},
    {0.96535, 5061},
    {0.95893, 38486},
    {0.94672, 77355},
    {0.94013, 155275},
    {0.94069, 233804},
    {0.94209, 311846},
    {0.94300, 390156},
    {0.94133, 468325},
    {0.94047, 518600},
    {0.94047, 999999},
  }
};


#endif // _HEADERFILE_H 
