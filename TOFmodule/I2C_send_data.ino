
void dataRequest() {  // ISR for data requests
#ifdef I2CDebug
  Serial.println("  Data requested... ");
#endif
  if (bSendArray != 0)
    I2CsendArray(); // bSendArray holds the channelof interest
  else
    I2Csend();
}


//************************* PREPARE DATA REQUEST ****************************
// This function loads the requested data and its size into a global variable ready for sending.
void prepareDataRequest() {
  uint8_t shift = 0; // This allows the opcode to be used to select the correct transducer channel
  #ifdef I2CDebug
  Serial.print("Preparing data... ");
  #endif
  bSendArray = 0; // default assumes an array is not requested

  // CASES WHERE DATA IS REQUESTED
  if (opcode >= 0x01 && opcode <= 0x04) { // TOF UPSTREAM TIME RAW DATA
    shift = 1;
    if (USTenable[opcode - shift] == HIGH) {  // is this sensor channel enabled?
      I2CbyteCount = 4;
      bSendArray = opcode;
#ifdef I2CDebug
      Serial.print("Pair "); Serial.print(opcode - shift + 1);
      Serial.println(" upstream raw data flagged for transfer");
#endif
    }
    else sensorNotInstalled(opcode - shift);
    return;
  }

  else if (opcode >= 0x05 && opcode <= 0x08) { // TOF DOWNSTREAM TIME RAW DATA
    shift = 5;
    if (USTenable[opcode - shift] == HIGH) {  // is this sensor channel enabled?
      I2CbyteCount = 4;
      bSendArray = opcode;
#ifdef I2CDebug
      Serial.print("Pair "); Serial.print(opcode - shift + 1);
      Serial.println(" downstream raw data flagged for transfer");
#endif
    }
    else sensorNotInstalled(opcode - shift);
    return;
  }

  else if (opcode >= 0x09 && opcode <= 0x0C) { //TOF UPSTREAM SD REQUESTS
    shift = 9;
    if (USTenable[opcode - shift] == HIGH) {  // is this sensor channel enabled?
#ifdef I2CDebug
      Serial.print("SD-UP pair "); Serial.print(opcode - shift + 1);  Serial.print(" = ");
      Serial.print(TOF[opcode - shift].UP_SD);  Serial.println(" ps");
#endif
      I2CbyteCount = 4; // How many bytes is the below?
      I2Cunion.Uvalue = TOF[opcode - shift].UP_SD; // 1st index [0] holds mean TOFdiff
    }
    else sensorNotInstalled(opcode - shift);
    return;
  }

  else if (opcode >= 0x0D && opcode <= 0x10) { // TOF DOWNSTREAM SD REQUESTS
    shift = 13;
    if (USTenable[opcode - shift] == HIGH) {  // is this sensor channel enabled?
#ifdef I2CDebug
      Serial.print("SD-DN pair "); Serial.print(opcode - shift + 1);  Serial.print(" = ");
      Serial.print(TOF[opcode - shift].DN_SD);  Serial.println(" ps");
#endif
      I2CbyteCount = 4; // How many bytes is the below?
      I2Cunion.Uvalue = TOF[opcode - shift].DN_SD; // 1st index [0] holds mean TOFdiff
    }
    else sensorNotInstalled(opcode - shift);
    return;
  }

  else if (opcode >= 0x11 && opcode <= 0x14) { // TOF DIFFERENCE SD REQUESTS
    shift = 17;
    if (USTenable[opcode - shift] == HIGH) {  // is this sensor channel enabled?
#ifdef I2CDebug
      Serial.print("SD-Diff pair "); Serial.print(opcode - shift + 1);  Serial.print(" = ");
      Serial.print(TOF[opcode - shift].diffSD);  Serial.println(" ps");
#endif
      I2CbyteCount = 4; // How many bytes is the below?
      I2Cunion.Uvalue = TOF[opcode - shift].diffSD; // 1st index [0] holds mean TOFdiff
    }
    else sensorNotInstalled(opcode - shift);
    return;
  }

  else if (opcode >= 0x15 && opcode <= 0x18) { // SUCCESS RATIO OF LAST TOF MEASUREMENT
    shift = 21;
    if (USTenable[opcode - shift] == HIGH) {  // is this sensor channel enabled?
#ifdef I2CDebug
      Serial.print("Success ratio pair "); Serial.print(opcode - shift + 1);  Serial.print(" = ");
      Serial.print(TOF[opcode - shift].successRatio);  Serial.println(" %");
#endif
      I2CbyteCount = 4; // How many bytes is the below? // 4 bytes is wrong, but a legacy thing.
      I2Cunion.Uvalue = TOF[opcode - shift].successRatio;

    }
    else sensorNotInstalled(opcode - shift);
    return;
  }

  else if (opcode >= 0x19 && opcode <= 0x1C) {  // TOF DIFF
    shift = 25;
    if (USTenable[opcode - shift] == HIGH) {  // is this sensor channel enabled?
#ifdef I2CDebug
      Serial.print("Arithmetic mean TOFdiff, pair "); Serial.print(opcode - shift + 1);  Serial.print(" = ");
      Serial.print(TOF[opcode - shift].diff);  Serial.println(" ps");
#endif
      I2CbyteCount = 4; // How many bytes is the below?
      I2Cunion.Svalue = TOF[opcode - shift].diff; // 1st index [0] holds mean TOFdiff
    }
    else sensorNotInstalled(opcode - shift);
    return;
  }

  else if (opcode >= 0x1D && opcode <= 0x20) {  // FLOW VELOCITY
    shift = 29;
    if (USTenable[opcode - shift] == HIGH) {  // is this sensor channel enabled?
      // FILL ME WITH CODE!
#ifdef I2CDebug
      Serial.print("Arithmetic mean flow velocity, pair "); Serial.print(opcode - shift + 1);  Serial.print(" = ");
      Serial.print(TOF[opcode - shift].flowVel_ums / 1000000, 2);  Serial.println(" m/s");
      Serial.print(TOF[opcode - shift].flowVel_ums);  Serial.println(" um/s");
#endif
      I2CbyteCount = 4; // How many bytes is the below?
      I2Cunion.Svalue = TOF[opcode - shift].flowVel_ums; // 1st index [0] holds mean TOFdiff
    }
    else sensorNotInstalled(opcode - shift);
    return;
  }

  else if (opcode >= 0x21 && opcode <= 0x24) {  // VOLUMETRIC FLOW
    shift = 33;
    if (USTenable[opcode - shift] == HIGH) {  // is this sensor channel enabled?
#ifdef I2CDebug
      Serial.print("Arithmetic mean flow volume, pair "); Serial.print(opcode - shift + 1);  Serial.print(" = ");
      Serial.print(TOF[opcode - shift].flowVol_uls / 1000000, 2);  Serial.println(" l/s ");
      Serial.print(TOF[opcode - shift].flowVol_uls);  Serial.println(" ul/s ");
#endif
      I2CbyteCount = 4; // How many bytes is the below?
      I2Cunion.Svalue = TOF[opcode - shift].flowVol_uls; // 1st index [0] holds mean TOFdiff
    }
    else sensorNotInstalled(opcode - shift);
    return;
  }

  else if (opcode >= 0x25 && opcode <= 0x28) {   // ZERO BASELINE
    shift = 37;
    if (USTenable[opcode - shift] == HIGH) {  // is this sensor channel enabled?
#ifdef I2CDebug
      Serial.print("Zero baseline, pair "); Serial.print(opcode - shift + 1);  Serial.print(" = ");
      Serial.print(TOF[opcode - shift].baseline);  Serial.println(" ps");
#endif
      I2CbyteCount = 4; // How many bytes is the below?
      I2Cunion.Svalue = TOF[opcode - shift].baseline;
    }
    else sensorNotInstalled(opcode - shift);
    return;
  }

  else if (opcode == 0x29) {  // COMPARATOR OFFSETS, DETECT, UPSTREAM
#ifdef I2CDebug
    Serial.print("Comparator upstream detect offsets = ");  Serial.print(" = ");
    for (int i = 0; i <= 3; i++) {
      Serial.print(TOF[i].comparatorOffset_det[0]); Serial.print(",  ");
    }
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    for (int i = 0; i < I2CbyteCount; i++) {
      I2Cunion.bytes[i] = TOF[i].comparatorOffset_det[0];
    }
    return;
  }

  else if (opcode == 0x2A) { // COMPARATOR OFFSETS, DETECT, DOWNSTREAM
#ifdef I2CDebug
    Serial.print("Comparator downstream detect offsets = ");  Serial.print(" = ");
    for (int i = 0; i <= 3; i++) {
      Serial.print(TOF[i].comparatorOffset_det[1]); Serial.print(",  ");
    }
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    for (int i = 0; i < I2CbyteCount; i++) {
      I2Cunion.bytes[i] = TOF[i].comparatorOffset_det[1];
    }
    return;
  }

  else if (opcode == 0x2B) { // COMPARATOR OFFSETS, RETURN, UPSTREAM
#ifdef I2CDebug
    Serial.print("Comparator upstreamstream return offsets = ");  Serial.print(" = ");
    for (int i = 0; i < I2CbyteCount; i++) {
      Serial.print(TOF[i].comparatorOffset_ret[0]); Serial.print(",  ");
    }
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    for (int i = 0; i < I2CbyteCount; i++) {
      I2Cunion.bytes[i] = TOF[i].comparatorOffset_ret[0];
    }
    return;
  }

  else if (opcode == 0x2C) { // COMPARATOR OFFSETS, RETURN, DOWNSTREAM
#ifdef I2CDebug
    Serial.print("Comparator downstream return offsets = ");  Serial.print(" = ");
    for (int i = 0; i <= 3; i++) {
      Serial.print(TOF[i].comparatorOffset_ret[1]); Serial.print(",  ");
    }
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    for (int i = 0; i <= 3; i++) {
      I2Cunion.bytes[i] = TOF[i].comparatorOffset_ret[1];
    }
    return;
  }

  else  if (opcode == 0x2D) { // FLUID DENSITY
#ifdef I2CDebug
    Serial.print("Density (grams per m3) = ");  Serial.print((uint32_t)(density * 1000.0));
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Uvalue = (uint32_t) (1000 * density); // convert to grams per cubic meter
    return;
  }

  else  if (opcode == 0x2E) { // FLUID VISCOSITY
#ifdef I2CDebug
    Serial.print("Viscosity (micro Cst) = ");  Serial.print((uint32_t)(viscosity * 1000000.0));
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Uvalue = (uint32_t) (1000000 * viscosity); // convert to micro CST
    return;
  }

  else if (opcode == 0x2F) {// FLUID SOUND SPEED
#ifdef I2CDebug
    Serial.print("Fluid sound-speed (mm/s) = ");  Serial.print((uint32_t)SoSf);
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Uvalue = (uint32_t)SoSf;
    return;
  }

  else if (opcode == 0x30) { // REYNOLDS NUMBER
#ifdef I2CDebug
    Serial.print("Reynolds number = ");  Serial.print(reynolds_number);
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Uvalue = reynolds_number; // convert to grams per cubic meter
    return;
  }

  else if (opcode == 0x31) {  // COMBINED VOLUME FLOW
#ifdef I2CDebug
    Serial.print("Combined flow =");  Serial.print(combinedFlow);  Serial.println(" ul/s");
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Svalue = combinedFlow;
    return;
  }

  else if (opcode == 0x32) { // COMBINED MASS FLOW
#ifdef I2CDebug
    Serial.print("Combined mass flow =");  Serial.print(massFlow);  Serial.println(" g/s");
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Svalue = massFlow;
    return;
  }

  else if (opcode == 0x33) { // TOTALISED VOLUME
#ifdef I2CDebug
    Serial.print("TOTALISED VOLUME =");  Serial.print(accFlowU.volDouble);  Serial.println(" L/s");
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Uvalue = (uint32_t)(accFlowU.volDouble);     
    return;
  }

  else if (opcode == 0x34) { // ACCUMULTION START TIME, IN UNIX SECONDS
#ifdef I2CDebug
    Serial.print("VOLUME ACCUMULATION STARTED AT ");  Serial.print(accTimeStart);  Serial.println(" unix seconds");
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Uvalue = accTimeStart;
    return;
  }
  
  else if (opcode == 0x36) { // VALUE OF PREVIOUS CONFIG CHANGE (FOR DOUBLE CHECK)
#ifdef I2CDebug
    Serial.print("Previous config value = ");  Serial.println(I2Ccheck);
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Uvalue = (uint32_t) I2Ccheck;
    return;
  }

  else if (opcode == 0x37) { // UPTIME
#ifdef I2CDebug
    Serial.print("Uptime is ");  Serial.print(millis() / 1000);  Serial.println(" seconds");
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Uvalue = (uint32_t) millis() / 1000;
    return;
  }

  else if (opcode == 0x38) { // SOFTWARE VERSION
#ifdef I2CDebug
    Serial.print("Software version is ");  Serial.println(software_version);
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    uint32_t value = (uint32_t) 1000 * software_version;
    I2Cunion.Uvalue = value;
    return;
  }

  else if (opcode == 0x3A) { // MODULE SERIAL NUMBER
#ifdef I2CDebug
    Serial.print("Serial number ");  Serial.println(serialNum_module);
#endif
    I2CbyteCount = 4; // How many bytes is the below?
    I2Cunion.Uvalue = serialNum_module;
    return;
  }

  invalidOpcodeRecieved();
}//********* END OF PREPARE DATA REQUEST *********


// Sends 8 readings (32 bytes) via I2C.
void I2CsendArray() {
  //bSendArray holds opcode.
  uint8_t p = 0; // the pair/channel being read
  boolean UPDN  = 0; // 0 when sending upstream and 1 for downstream
  if (bSendArray > 0 && bSendArray <= 4) { // upstream data requests (1,2,3,4 allowed)
    p = bSendArray - 1;
    UPDN  = 0;
  }
  if (bSendArray > 4 && bSendArray <= 8) { // downstream data requests (5,6,7,8 allowed)
    p = bSendArray - 5;
    UPDN  = 1;
  }

  uint8_t transferSize = 8;
  for (uint8_t i = TOF[p].rawIndex[UPDN]; i < (TOF[p].rawIndex[UPDN] + transferSize); i++) { // send more readings
    if (UPDN == 0)  // upstream data requests
      I2Cunion.Uvalue = TOF[p].UP[i];
    if (UPDN == 1)  // downstream data requests
      I2Cunion.Uvalue = TOF[p].DN[i];

#ifdef I2CDeepDebug
    Serial.print("Transducer channel ");  Serial.print(p + 1);  Serial.print(" - ");
    Serial.print("Measurement ");  Serial.print(i + 1);
    Serial.print(". Sending ");  Serial.print(I2Cunion.Uvalue);  Serial.println(" ps... ");
#endif
    I2Csend();
  } // iterate to next measurement
  TOF[p].rawIndex[UPDN] = TOF[p].rawIndex[UPDN] + transferSize;

} // END OF SEND ARRAY


// PROTOCOL FOR SENDING UP TO 4 BYTES VIA I2C
void I2Csend() {
  //for (int b = (I2CbyteCount - 1); b >= 0; b--) {   // sending  MOST significant byte first
  for (int b = 0; b < I2CbyteCount; b++) { // sending  LEAST significant byte first
    Wire.write(I2Cunion.bytes[b]);
#ifdef I2CDeepDebug
    Serial.print("Byte ");  Serial.print(b); Serial.print(" = "); print_byte(I2Cunion.bytes[b]);  Serial.println(" sent");
#endif
  }
#ifdef I2CDeepDebug
  Serial.println();
#endif
  digitalWrite(pSCL, HIGH); // pin 19 is SCL
}
