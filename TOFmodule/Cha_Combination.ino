

void Cha_combination() {
  Serial.println();  Serial.println();
  update_flow_logs(); // update flowLog and qualityLog
  adjust_weightings(); // adjust each channels weightings based on their quality scores (weight becomes 0 for failed reading)
  running_average_flow(); // calculate each channels weighted running average flow
  find_agreement();   // calc total quality score for each channel. Determine which channels will be accepted.
  combine_channels();  // combine channels on their total quality
}

// UPDATE FLOW LOGS FOR EACH CHANNEL
void update_flow_logs() {
  // updates flowLog and qualityLog
  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.

    TOF[p].totalQuality = 0; // reset total quality
    for (uint8_t i = (flow_logSize - 1); i >= 1; i--) { // shift all flow/quality values 9 into 10, 5 into 6, 0 into 1 etc
      TOF[p].flowLog[i]    = TOF[p].flowLog[i - 1];
      TOF[p].qualityLog[i] = TOF[p].qualityLog[i - 1];
      TOF[p].totalQuality  += def_weights[i] * TOF[p].qualityLog[i]; // recalculate total quality
    }

    TOF[p].flowLog[0]    = TOF[p].flowVol_uls;                         // load most recent flow value into index 0 of log

    if (TOF[p].diffSD == 0 || TOF[p].successRatio < cha_cutoff) { // if measurement was a failure
      TOF[p].qualityLog[0] = 0;
    }
    else { // calc and save most recent quality value. High success ratio, and low SD both good.
      TOF[p].qualityLog[0] = (float)TOF[p].successRatio * 10000  / (float)TOF[p].diffSD;
    }

    TOF[p].totalQuality  = TOF[p].totalQuality + (def_weights[0] * TOF[p].qualityLog[0]);    // finally, include quality from most recent measurement

  }
}


// ************************** ADJUST WEIGHTINGS **************************
// Each channel is adjusted in isolation
void adjust_weightings() {
  double weights_sum = 0; // used to
  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.

    weights_sum = 0; // reset this for next channel

    for (uint8_t i = 0; i < flow_logSize; i++) {
      if (TOF[p].qualityLog[i] < 1) {
        TOF[p].weights[i] = 0;
      }
      else {
        TOF[p].weights[i] = def_weights[i] * TOF[p].qualityLog[i] / TOF[p].totalQuality; // scales run-ave weighting based on that measurements contribution to total quality
      }
      weights_sum = weights_sum + TOF[p].weights[i]; // accumulate the total
    }

    // ENSURE ADJUSTED WEIGHTS SUM TO 1
    double adjustment = 0.0;  // adjustment exists to ensure the weights always sum to 1
    adjustment = float(1.0) / weights_sum;
    if (weights_sum == 0) { // otherwise adjustment = inf, and weights = nan
      adjustment = 0;
    }
    else {
      //Serial.print("adjust = "); Serial.print(adjustment);
      for (uint8_t i = 0; i < flow_logSize; i++) {
        TOF[p].weights[i] = TOF[p].weights[i] * adjustment;
      }
      weights_sum = weights_sum * adjustment; // scale the sum too
#ifdef errorDebug
      if (weights_sum < 0.999 || weights_sum > 1.001) {
        Serial.println();
        Serial.print("Weight sum = "); Serial.println(weights_sum, 6);
        Serial.print("!!!! weights didn't sum to 1 !!!!");
      }
#endif
    }

#ifdef flowDebug
    Serial.println();
    Serial.println();
    Serial.print("Channel "); Serial.print(p + 1);  Serial.print(" total quality = "); Serial.println(TOF[p].totalQuality);
    Serial.print("Measurement:   ");
    for (uint8_t i = 0; i < flow_logSize; i++) {
      Serial.print(i + 1);  Serial.print("      ");
    }
    Serial.println();  Serial.print("Flow:          ");
    for (uint8_t i = 0; i < flow_logSize; i++) {
      Serial.print((float)TOF[p].flowLog[i] / 1000000, 1);  Serial.print("  ");
    }
    Serial.println();  Serial.print("Quality:       ");
    for (uint8_t i = 0; i < flow_logSize; i++) {
      Serial.print(TOF[p].qualityLog[i]);  Serial.print("   ");
    }

    Serial.println();  Serial.print("Weighting:     ");
    for (uint8_t i = 0; i < flow_logSize; i++) {
      Serial.print(100 * TOF[p].weights[i], 1);  Serial.print("    ");
    }
    Serial.println();
#endif

  } // iterate over channels

} // END OF ADJUST WEIGHTINGS


void running_average_flow() {
  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.

    for (uint8_t i = 0; i < flow_logSize; i++) {
      TOF[p].aveFlow_uls = TOF[p].aveFlow_uls + (TOF[p].weights[i] * TOF[p].flowLog[i]);
    }
  }
}

//identifies the following situations...
// all channels agree
// two channels agree, but disagree with another two which do agree
// flow is very low, making a simple percentage range for agreement impractical.
// only 1 channel enabled
// one channel disagrees with all, but has dramatically better quality (many failing one good channel)
void find_agreement() {
  if (activePairs <= 1) return;  // not required if only 1 pair active

  // WHICH CHANNELS AGREE? Channels always agree with themselves
  //stored as matrix in TOF.agreement[]
#ifdef flowDebug
  Serial.println();  Serial.println("         1  2  3  4");
#endif
  for (int p = 0; p < SC_TOF; p++) { // the primary pair being compared is p
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.

    TOF[p].accepted = HIGH; // default is to use all channels
    TOF[p].agreeCount = 0;
    memset(TOF[p].agreement, LOW, sizeof(TOF[p].agreement));  // reset agreement matrix. assume no agreements to start
#ifdef flowDebug
    Serial.print("Cha ");  Serial.print(p + 1);  Serial.print(" | ");
#endif

    float bounds = 5; // % allowed either way for channel agreement
    float lowFlow = 10000000; // Flow below which the bounds are expanded. in micro litres per second.
    int32_t flatBound = 3000000; // 3 litres per second
    if ((TOF[p].successRatio >= 50) && (TOF[p].aveFlow_uls <= lowFlow)) {
      //Serial.println("Potential low-flow scenario identified...");
      bounds = bounds * 2;
      //Serial.print("Bounds for channel ");  Serial.print(p+1);  Serial.print(" = ");  Serial.print(bounds);
    }

    // MAKING THE AGREEMENT MATRIX
    for (int c = 0; c < SC_TOF; c++) { // c is the pair being compared to p
      // IF the flow is within the bound percent of the compared to channel, OR if the success ratio is good, and flow is low, then a much wider bound is permitted.
      if ( (( TOF[c].aveFlow_uls > ((1 - (bounds / 100)) * TOF[p].aveFlow_uls)) && (TOF[c].aveFlow_uls < (1 + (bounds / 100)) * TOF[p].aveFlow_uls))
           || ((TOF[p].aveFlow_uls > (TOF[c].aveFlow_uls - flatBound)) && (TOF[p].aveFlow_uls < (TOF[c].aveFlow_uls + flatBound))) ) {
        TOF[p].agreement[c] = HIGH; // if agree, then make the corrasponding bit high
        TOF[p].agreeCount++;
#ifdef flowDebug
        Serial.print(" 1 ");
#endif
      }
#ifdef flowDebug
      else Serial.print(" 0 ");
#endif
    }
    Serial.println();
  }


  // IF ALL CHANNELS AGREE
  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue;  // skip disabled channels


    if (TOF[p].agreeCount >= (SC_TOF)) {
      // default is accept all, so break.
#ifdef coreDebug
      Serial.println("** ALL CHANNELS AGREE **");
#endif
      return;
    }
#ifdef flowDebug
    Serial.print("Cha"); Serial.print(p + 1);  Serial.print(" agree count = "); Serial.println(TOF[p].agreeCount);
#endif
  }


  // HOW MANY CHANNELS TOTALLY DISAGREED?
  uint8_t dis_counter = 0;
  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue;  // skip disabled channels
    if (TOF[p].agreeCount <= 1) {  // channel only agrees with itself
      TOF[p].accepted = LOW; // mark the channel as disagreed
      dis_counter++;  // how many channels disagree with all?
    }
  }
#ifdef flowDebug
  Serial.print(dis_counter);  Serial.println(" totally disagree");
#endif



  // SINGLE DISAGREEING CHANNEL REPRESENTS MAJORITY OF QUALITY
  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue;  // skip disabled channels

    if (TOF[p].accepted == LOW) {  // identify disagreing channel(s)
      // if disagreing channel accounts for 60%+ of total quality then accept that instead
      if (TOF[p].totalQuality > 0.6 * (TOF[0].totalQuality + TOF[1].totalQuality + TOF[2].totalQuality + TOF[3].totalQuality) ) {
#ifdef coreDebug
        Serial.print("Cha "); Serial.print(p + 1); Serial.println(" represents majority of total measurement quality... Selecting only that channel.");
#endif
        for (int c = 0; c < SC_TOF; c++) {
          if (USTenable[p] == LOW) continue;  // skip disabled channels
          if (c == p)
            TOF[c].accepted = HIGH;
          else
            TOF[c].accepted = LOW;
        }
      }
    }
  }


  // TWO PAIRS OF AGREEING CHANNELS WHICH DISAGREE WITH EACH OTHER
  // this state can only ever happen with 4 transducers
  uint8_t set1[2] = {0};
  uint8_t set2[2] = {0};
  if (dis_counter == 0 && TOF[0].agreeCount && TOF[1].agreeCount && TOF[2].agreeCount && TOF[3].agreeCount) {
#ifdef coreDebug
    Serial.println("Two sets of agreeing channels identified");
#endif
    // which channels are in which agreement set?
    for (int c = 1; c < 4; c++) {
      if (TOF[0].agreement[c] == HIGH) {
        set1[0] = 0; // cha 1 always agrees with itself
        set1[1] = c;
        if (c == 1) {  // set 1 is cha1 & cha2
          set2[0] = 2;
          set2[1] = 3;
        }
        if (c == 2) {  // set 1 is cha1 & cha3
          set2[0] = 1;
          set2[1] = 3;
        }
        if (c == 3) { // set 1 is cha1 & cha4. set 2 is cha2 & cha3
          set2[0] = 1;
          set2[1] = 2;
        }
      }
    }

    // which set has the highest total quality?
    float qualTemp1 = TOF[set1[0]].totalQuality + TOF[set1[1]].totalQuality;
    float qualTemp2 = TOF[set2[0]].totalQuality + TOF[set2[1]].totalQuality;
#ifdef flowDebug
    Serial.print("First set contains channels ");  Serial.print(set1[0] + 1);  Serial.print(" and ");  Serial.print(set1[1] + 1); Serial.print(", with total quality = ");  Serial.println(qualTemp1);
    Serial.print("Second set contains channels ");  Serial.print(set2[0] + 1);  Serial.print(" and ");  Serial.print(set2[1] + 1); Serial.print(", with total quality = ");  Serial.println(qualTemp2);
#endif

    if (qualTemp1 > qualTemp2) { // disable the worst set
#ifdef flowDebug
      Serial.println("First set selected");
#endif
      TOF[set2[0]].accepted = LOW;
      TOF[set2[1]].accepted = LOW;
    }
    else {
#ifdef flowDebug
      Serial.println("Second set selected");
#endif
      TOF[set1[0]].accepted = LOW;
      TOF[set1[1]].accepted = LOW;
    }
  }


  // EVERY CHANNEL DISAGREES - just pick channel with best total quality score.
  if ( dis_counter == SC_TOF) { // if disagreement counter = sensor count then run below code, otherwise job done and return.
    // select channel with best quality score
    float qualTemp = TOF[0].totalQuality;  // start comparing with cha 1
    uint8_t pairTemp = 0;  // default to channel 1
    for (int p = 1; p < SC_TOF; p++) {
      if (TOF[p].totalQuality > qualTemp) {
        qualTemp = TOF[p].totalQuality;
        pairTemp = p;
      }
    }
#ifdef coreDebug
    Serial.print("Cha ");  Serial.print(pairTemp + 1);   Serial.println(" has best total quality score and has been selected");
#endif
    TOF[pairTemp].accepted = HIGH; // accept the best perfroming channel.
  }

} // END OF FIND AGREEMENT



void combine_channels() {
  combinedFlow = 0; // reset just in case
  float allQual = 0;
  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW || TOF[p].accepted == LOW) continue; // skip current pair if it has been disabled or disagreed.
    allQual += TOF[p].totalQuality;
  }

  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue; // skip current pair if it has been disabled.
    if (TOF[p].accepted == HIGH) {
      float proportion = TOF[p].totalQuality / allQual;
      combinedFlow += TOF[p].aveFlow_uls * proportion;
#ifdef coreDebug
      Serial.print("Channel ");  Serial.print(p + 1);  Serial.print(" approved for combination with flow = "); Serial.print((float)TOF[p].aveFlow_uls / 1000000, 3);
      Serial.print(" L/s, contributing ");  Serial.print(100 * proportion);  Serial.println(" %");
#endif
    }
  }
#ifdef coreDebug
  Serial.print("****   Combined flow = ");  Serial.print((float)combinedFlow / 1000000, 2);  Serial.println("   ****");
#endif
}
