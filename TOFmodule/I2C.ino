// MANAGES COMMUNICATIONS WITH AN EXTERNAL CONTROLLER VIA I2C. DEFAULT ADDresS IS 22
// CURRENTLY READS AND WRITES LEAST SIGNIFICANT BYTE FIRST

//********************** RECIEVE EXTERNAL OPCODE COMMAND *************************
//********************************************************************************
void receiveInstruction(int byteCount) { // interrupt service routine for recieving data
  I2C_instructionRecieved = byteCount; // flags instruction is pending and holds size of sent data
  opcode = Wire.read(); // save command for later use or if a data request is triggered

#ifdef I2CDebug
  Serial.println();
  Serial.println();
  Serial.print("Instruction recieved.  Byte count is ");  Serial.print(I2C_instructionRecieved);
  Serial.print(".  Command 0x");  Serial.print(opcode, HEX);  Serial.print(" identified...  ");
#endif

  if (opcode == 0x42) {
#ifdef errorDebug
    Serial.println();  Serial.println();
    Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    Serial.println("!!  Soft Reset via I2C... Rebooting...  !!");
    Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
#endif
    reboot();
  }

  if (opcode == 0x39) { // READ STATUS REGISTER  data is loaded as part of the interrupt, just in case.
#ifdef I2CDebug
    printStatus();
#endif
    I2CbyteCount = 2; // How many bytes is the below?
    I2Cunion.Uvalue = statusReg;

    if (bitRead(statusReg, TOFprog) == HIGH) { // currently measuring flow?
      statusReg = 0;
      bitWrite(statusReg, TOFprog, HIGH); // maintain in progress status bit
    }
    else {
      statusReg = 0; // reset the register after each read
    }
    digitalWrite(pINT_module, HIGH);  // reset module's INT pin
    //Serial.println("************************************ INT PIN IS DEASSERTED ************************************");
    I2C_instructionRecieved = 0; // don't needlessly trigger functions to action the opcode again
    opcode = 0; // opcode complete, so reset it.
  }
}


boolean readInstruction() {
  if (I2C_instructionRecieved == 1) {
    return 0;  // flag that an opcode command or data read is now pending
  }

  else { // more than 1 byte means a config change must be pending
#ifdef I2CDebug
    Serial.println("configuration change pending...");
#endif

    memset((void *)bytesRecieved, 0, sizeof(bytesRecieved)); // ensure array is clear

    for (int i = 0; i < (I2C_instructionRecieved - 1) ; i++) { // expects LEAST SIGNIFICANT BYTE FIRST.
      //for (int i = (I2C_instructionRecieved - 2); i >= 0 ; i--) { // expects MOST SIGNIFICANT BYTE FIRST.
      bytesRecieved[i] = Wire.read();
#ifdef I2CDeepDebug
      Serial.print("Byte ");  Serial.print(i); Serial.print(" = "); print_byte(bytesRecieved[i]);  Serial.println();
#endif
    }
#ifdef I2CDebug
    Serial.println("Recieved for config change");
#endif
    return 1; // flag that a config change is now pending
  }
}

void actionOpcode() {
#ifdef I2CDebug
  Serial.print("Executing instruction 0x");  Serial.println(opcode, HEX);
#endif
  switch (opcode) {
    // *** CASES WHICH DO NOT REQUIRE DATA TRANSFER ***
    case 0x41: // TOF measurement with all pairs
      bStartTOF = HIGH;
#ifdef I2CDebug
      Serial.println("Flow measurement commanded and pending... ");
#endif
      return;

    case 0x42: // Soft Reset operation. This is handled directly by the interrupt.
      return;

    case 0x43: // Take a baseline measurement
#ifdef I2CDebug
      Serial.println("Performing ultrasonic ToF baseline measurement...");
#endif
      digitalWrite(pINT_module, LOW); // assert int pin
      baselineTOF();
      saveCalibration();
      bitWrite(statusReg, BLcal, HIGH); // done calibrating or baselining
      digitalWrite(pINT_module, LOW); // assert int pin
      //Serial.println("************************************ INT PIN ASSERTED ************************************");
      printStatus();
      return;

    case 0x44: // Perform comparator voltage baseline calibration
#ifdef I2CDebug
      Serial.println("Calibrating voltage offset on comparators...");
#endif
      digitalWrite(pINT_module, LOW); // assert int pin
      calibrate_comparators();
      saveCalibration();
      bitWrite(statusReg, BLcal, HIGH); // done calibrating or baselining
      digitalWrite(pINT_module, LOW); // assert int pin
      //Serial.println("************************************ INT PIN ASSERTED ************************************");
      printStatus();
      return;

    case 0x45: // Enter low power mode
      digitalWrite(pINT_module, LOW); // assert int pin
      for (int p = 0; p < SC_TOF; p++)
        digitalWrite(pAF_EN[p], LOW);
#ifdef I2CDebug
      Serial.println("Entered low-power mode.");
#endif
      return;

    case 0x46: // Leave low power mode
      digitalWrite(pINT_module, LOW); // assert int pin
      setPowerModes();
#ifdef I2CDebug
      Serial.println("Exited low-power mode.");
#endif
      return;

    case 0x47: // load test data
      digitalWrite(pINT_module, LOW); // assert int pin
      I2C_load_test_data();
      return;

    case 0x48: // Restart accumulating flow (erases EEPROM).
      digitalWrite(pINT_module, LOW); // assert int pin
      accFlowU.volDouble = 0;
      clearEEPROM();
      saveCalibration();
      EEPROM_initialisation();
#ifdef I2CDebug
      printEEPROM();
      Serial.println("Volume accumulation has restarted.");
#endif
      return;

    case 0x49: // Save current comparator calibration to EEPROM
      digitalWrite(pINT_module, LOW); // assert int pin
      saveCalibration();
#ifdef I2CDebug
      Serial.println("Current comparator calibration has been saved to EEPROM.");
#endif
      return;

    case 0x50: // Read comparator calibration from EEPROM and upload to MAX35 ICs
      digitalWrite(pINT_module, LOW); // assert int pin
      loadCalibrationEEPROM();
#ifdef I2CDebug
      Serial.println("Comparator calibration has been restored from EEPROM");
#endif
      return;

    default:
      prepareDataRequest(); // if none of the above then it's a data request (or error dealt with later)
      break;
  }
}



void sensorNotInstalled(uint8_t p) {
  //bitWrite(statusReg, I2CdataError, HIGH);
  //bitWrite(statusReg, p, HIGH);
  //digitalWrite(pINT_module, LOW);
  I2CbyteCount = 4; // just fill the I2C transmit with 0s
  I2Cunion.Uvalue = 0;;
#ifdef I2CDebug
  Serial.println("!! This sensor is not enabled !!");
#endif
}


void invalidOpcodeRecieved() {
#ifdef errorDebug
  Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  Serial.println("! Invalid opcode recieved !");
  Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!");
#endif
  bitWrite(statusReg, invalidOpcode, HIGH);
  digitalWrite(pINT_module, LOW);
  opcode = 0;
  I2CbyteCount = 4; // I2C panic state is 00001111,00001111,00001111,00001111
  I2Cunion.Uvalue = I2Cpanic; // 2863311530
}


void I2C_load_test_data() {
  Serial.print(" LOADING EXAMPLE TEST DATA... ");
  serialNum_module = 12345;
  statusReg = 0B1010101010101010;
  combinedFlow = -11111; // holds the combined running weighted average flow in ul/s
  massFlow = -22222;
  density = 3.333; // stored as kg/m3, sent as g/m3. So expect 3333
  viscosity = 4.444; // stored as CST, sent as microCST, so expect 4444000
  SoSf = 5555;
  reynolds_number = 6666;

  for (uint8_t p = 0; p < 4; p++) {
    clearResults(p);
    for (uint8_t i = 0; i < 100; i++) {
      TOF[p].UP[i]                 = 1000 + i * (p + 1);
      TOF[p].DN[i]                 = 2000 + (i * (p + 1));
    }

    TOF[p].UP_SD                   = 4 * (p + 1);
    TOF[p].DN_SD                   = 5 * (p + 1);

    TOF[p].diff                    = 100 * (p + 1); // [0] is arithmetic mean. All in ps.
    TOF[p].diffSD                  = 6 * (p + 1);
    TOF[p].successRatio            = 50 + (10 * (p + 1)); // ratio of i (successful measurements) to total measure attempts (100 means every measurement succeeded).

    TOF[p].baseline                = 8 * (p + 1); // this baseline (ps) is SUBTRACTED from all TOFdifferences AVERAGES.
    TOF[p].comparatorOffset_det[0] = 5 * (p + 1); // baseline applied to MAX35 internal compatator. Between 0 and 127. [0] is UP [1] is DN
    TOF[p].comparatorOffset_det[1] = 8 * (p + 1); // baseline applied to MAX35 internal compatator. Between 0 and 127. [0] is UP [1] is DN
    TOF[p].comparatorOffset_ret[0] =  -5 * (p + 1); // baseline applied to MAX35 internal compatator. Between -127 and 127. [0] is UP [1] is DN
    TOF[p].comparatorOffset_ret[1] =  -8 * (p + 1); // baseline applied to MAX35 internal compatator. Between -127 and 127. [0] is UP [1] is DN

    TOF[p].flowVel_ums             = 20 * (p + 1) + 5000; // flow velocity in micro meters per second, from arithmetic tof mean
    TOF[p].flowVol_uls             = 20 * (p + 1) + 4000; // flow volume in micro litres per second
  }
  Serial.println(" DONE.");
}


// PRINTS BYTES WITHOUT IGNORING 0S AT THE END
void print_byte(byte b) {
  for (int i = 7; i >= 0; i--)
    Serial.print(bitRead(b, i));
}
