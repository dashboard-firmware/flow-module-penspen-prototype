// THIS SET OF FUNCTIONS INTERFACES WITH THE MAX35 CHIP TO MANAGE TIME-OF-FLIGHT MEASUREMENTS
void interruptMAX0() {
  interruptMAX(0);
}
void interruptMAX1() {
  interruptMAX(1);
}
void interruptMAX2() {
  interruptMAX(2);
}
void interruptMAX3() {
  interruptMAX(3);
}

void interruptMAX(int p) { // Handles interrupts from MAX35104 SoCs
  intReg_MAX[p] = 0;
  intReg_MAX[p] = readFromMAX(p, 0xFE);
  if (intReg_MAX[p] != 0) {  // Update any non-zero interrupt data
    if (bitRead(intReg_MAX[p], 12) == HIGH) // update measuremrnt complete flag
      bTOFcompleteFlag[p] = HIGH;
    else
      bTOFcompleteFlag[p] = LOW;
    if (bitRead(intReg_MAX[p], 15) == HIGH) // update measuremrnt complete flag
      bTOFtimeoutFlag[p] = HIGH;
    else
      bTOFtimeoutFlag[p] = LOW;
  }
#ifdef INTDebug   // only print if printing enabled
  Serial.print(" ! MAX Interrupt ! ");
  printInterruptMAX(p);
#endif
}


#ifdef INTDebug
void printInterruptMAX(uint8_t p) {
  Serial.print("  Cha ");  Serial.print(p + 1);  Serial.print(" int-reg: ");
  printBIN16(intReg_MAX[p]);

  if (bitRead(intReg_MAX[p], 15) == HIGH)
    Serial.print(" - !! TOF timeout occured !! ");
  if (bitRead(intReg_MAX[p], 14) == HIGH)
    Serial.print(" - Alarm Flag ");
  if (bitRead(intReg_MAX[p], 13) == HIGH)
    Serial.print(" - reserved bit ");
  if (bitRead(intReg_MAX[p], 12) == HIGH)
    Serial.print(" - TOF measurement completed ");
  if (bitRead(intReg_MAX[p], 11) == HIGH)
    Serial.print(" - Temperature command completed ");
  if (bitRead(intReg_MAX[p], 10) == HIGH)
    Serial.print(" - Internal LDO stabilised ");
  if (bitRead(intReg_MAX[p], 9) == HIGH)
    Serial.print(" - Event timing TOF completed ");
  if (bitRead(intReg_MAX[p], 8) == HIGH)
    Serial.print(" - Event timing temp completed ");
  if (bitRead(intReg_MAX[p], 7) == HIGH)
    Serial.print(" - reserved bit ");
  if (bitRead(intReg_MAX[p], 6) == HIGH)
    Serial.print(" - Calibrate command completed ");
  if (bitRead(intReg_MAX[p], 5) == HIGH)
    Serial.print(" - Halt command completed ");
  if (bitRead(intReg_MAX[p], 4) == HIGH)
    Serial.print(" - Case tamper detected ");
  if (bitRead(intReg_MAX[p], 3) == HIGH)
    Serial.print(" - reserved bit ");
  if (bitRead(intReg_MAX[p], 2) == HIGH)
    Serial.print(" - Power-On-reset ");
  if (bitRead(intReg_MAX[p], 1) == HIGH)
    Serial.print(" - reserved bit ");
  if (bitRead(intReg_MAX[p], 0) == HIGH)
    Serial.print(" - reserved bit ");
  //Serial.println();
}
#endif


void clearResults(uint8_t p) { // clears flow results registers
#ifdef TOFDeepDebug
  Serial.print("Clearing resuluts registers for Cha ");  Serial.println(p+1);
#endif
  TOF[p].UP_SD   = 0;
  TOF[p].DN_SD   = 0;
  TOF[p].diff = 0;
  TOF[p].diffSD = 0;
  TOF[p].successRatio = 0;

  TOF[p].flowVel_ms  = 0;  // flow velocity in meters per second.
  TOF[p].flowVel_ums = 0;  // flow velocity in micro meters per second, from arithmetic tof mean
  TOF[p].flowVol_ls  = 0;  // flow volume in litres per second
  TOF[p].flowVol_uls = 0;  // flow volume in micro litres per second
  TOF[p].flowMass_gs = 0;  // flow mass in grams per second
  TOF[p].aveFlow_uls    = 0;

  memset(TOF[p].UP, 4294967295, sizeof(TOF[p].UP)); // 256 array elements with 4 bytes each = 1024
  memset(TOF[p].DN, 4294967295, sizeof(TOF[p].DN)); // by default this array is all 1s (4294967295).
  memset(TOF[p].diffLogF, 0, sizeof(TOF[p].diffLogF));
  memset(TOF[p].rawIndex, 0, sizeof(TOF[p].rawIndex)); // 2 elements 1 byte each
} // END OF CLEARING RESULTS



//**************************** WRITING TO THE MAX35 *****************************
//*******************************************************************************
void writeToMAX(uint8_t pairNum, uint8_t address, uint16_t value, boolean op_code) {
#ifndef simulation_mode
  SPI.beginTransaction(MAX_SPI);
  digitalWrite((pCS_MAX[pairNum]), LOW);  delayMicroseconds(10);      // start communication
  SPI.transfer(address);            // address of register or OPCODE
  if (op_code != HIGH)              // Don't send data in the case of OPCODE
    SPI.transfer16(value);          // Send 2 bytes if this isn't an opcode only write
  digitalWrite((pCS_MAX[pairNum]), HIGH);     // end communication
  SPI.endTransaction();
#else
  delayMicroseconds(50);
#endif
} //*********************** END OF WRITING TO THE MAX35 *************************


//*************************** READING FROM THE MAX35 ****************************
//*******************************************************************************
uint16_t readFromMAX(uint8_t pairNum, uint8_t address) {
#ifndef simulation_mode
  SPI.beginTransaction(MAX_SPI);
  digitalWrite((pCS_MAX[pairNum]), LOW);  delayMicroseconds(10);       // start communication
  SPI.transfer(address);               // address to read
  uint16_t result = SPI.transfer16(0); // read 2 unsigned bytes
  digitalWrite((pCS_MAX[pairNum]), HIGH); delayMicroseconds(10);      // end communication
  SPI.endTransaction();
  return result;
#else
  delayMicroseconds(50);
  return 0;
#endif
} //************************ END READING FROM THE MAX35 *************************


//******************* READING SIGNED INTEGERS FROM THE MAX35 ********************
//*******************************************************************************
int16_t readFromMAX_signed(uint8_t pairNum, uint8_t address) { // for the cases in which values are returned as signed integers
#ifndef simulation_mode
  SPI.beginTransaction(MAX_SPI);
  digitalWrite((pCS_MAX[pairNum]), LOW);  delayMicroseconds(10);    // start communication
  SPI.transfer(address);              // address to read
  int16_t result = SPI.transfer16(0); // read 2 signed bytes
  digitalWrite((pCS_MAX[pairNum]), HIGH); delayMicroseconds(10);      // end communication
  SPI.endTransaction();
  return result;
#else
  delayMicroseconds(50);
  return 0;
#endif
} //************** END OF READING SIGNED INTEGERS FROM THE MAX35 ****************
