// if a config change is pending then opcode should point to one of these config updates
// ReadInstruction will have already loaded the recieved data into bytesRecieved.
// This switch case simply uses the opcode to match the correct config register to the freshly recieved data



// COMBINES UP TO 4 BYTES RECIEVED VIA I2C
uint32_t combineBytes(uint8_t b) {  // b denotes which bytes to combine. e.g 00001100 is bytes 3 and 4.
#ifdef I2CDeepDebug
  Serial.println("combining bytes");
#endif
  uint32_t value = 0;
  for (int i = 3; i >= 0; i--) { // iterate over 4 times
    if (bitRead(b, i) == HIGH) { // if this is a byte to be combined
      value = (value << 8) | bytesRecieved[i];
#ifdef I2CDeepDebug
      Serial.print(" byte "); Serial.print(i);  Serial.print(" is HIGH - ");
      Serial.print(" recieved value is now ");  Serial.print(value, BIN);  Serial.print(" = ");  Serial.println(value);
#endif
    }
  }
  return value;
}// END OF COMBINE BYTES


// RE-GENERATES AND UPLOADS CONFIGURATION REGISTERS TO EVERY MAX35 IC
void I2C_updateConfig(boolean MAX_involved, float value) {
  if (MAX_involved == HIGH) {
    generate_MAXconfig();
    upload_MAXconfig(-1);
    //TODO check this worked
  }
  I2Ccheck = value; // remembers set value to allow master to double check. (opcode 0x36 as on V0.961)

  bitWrite(statusReg, configSuccess, HIGH);
  digitalWrite(pINT_module, LOW);
  //Serial.println("************************************ INT PIN ASSERTED ************************************");
}


// CHECKS TO ENSURE NEW CONFIG VALUES ARE WITHIN PRE-SET UPPER AND LOWER LIMITS
boolean checkNewConfig(float value, float LB, float HB) {
  if (value >= LB && value <= HB) {
    return HIGH;
  }
  else {
    bitWrite(statusReg, configIssue, HIGH);
    digitalWrite(pINT_module, LOW); // assert int pin
#ifdef I2CDebug
    Serial.println("!!!! Parameter change out of bounds !!!!");
    Serial.print("Recieved value is ");  Serial.println(value);
    Serial.print("Lower bound = "); Serial.print(LB); Serial.print("    Upper bound = "); Serial.println(HB);
#endif
    return LOW;
  }
}


void configChange() {
  // *** CASES WHICH ACCEPT EXTERNAL DATA ***
#ifdef I2CDebug
  Serial.println("Updating config... ");
#endif
  float value = 0;

  if (opcode == 0x61) { // Accept latest temperature measurements. // 1st and 2nd bytes.
    value = (int16_t)combineBytes(0B00000011);
    if (value >= LBtemperature && value <= HBtemperature) { // if within bounds
      temperature = (int16_t)value;
      I2C_updateConfig(LOW, temperature); // MAX35 update required? Signed? Byte count? config value
    }
    else { // OUT OF BOUNDS
      bitWrite(statusReg, configIssue, HIGH);
      digitalWrite(pINT_module, LOW); // assert int pin
#ifdef I2CDebug
      Serial.println("!!!! Parameter change out of bounds !!!!");
      Serial.print("Recieved value is ");  Serial.println(value);
      Serial.print("Lower bound = "); Serial.print(LBtemperature); Serial.print("    Upper bound = "); Serial.println(HBtemperature);
#endif      // below sets temp to the bound limit.
      if (value < LBtemperature)
        temperature = LBtemperature;
      if (value > HBtemperature)
        temperature = HBtemperature;
    }
#ifdef I2CDebug
    Serial.print("Temperature set to ");  Serial.print((float)temperature / 100);  Serial.println(" centigrade");
#endif
    return;
  }

  else if (opcode == 0x63) { // BASELINE MEASUREMENT DURATION
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBbaselineTime, HBbaselineTime) == HIGH) {
      baselineTime = (uint8_t)bytesRecieved[0];
      I2C_updateConfig(LOW, baselineTime); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("TOF baseline duration is ");  Serial.print(baselineTime);  Serial.println(" seconds.");
#endif
    return;
  }

  else if (opcode == 0x64) { // COMPARATOR CALIBRATION DURATION
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBcompCalTime, HBcompCalTime) == HIGH) {
      compCalTime = (uint8_t)value;
      I2C_updateConfig(LOW, compCalTime); // MAX35 update required? Signed? Byte count? config value
    }
#ifdef I2CDebug
    Serial.print("New comparator calibration duration is ");  Serial.print(compCalTime);  Serial.println(" seconds.");
#endif
    return;
  }

  else if (opcode == 0x65) { // COMPARATOR CALIBRATION RESOLUTION
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBres, HBres) == HIGH) {
      res = (uint8_t)value;
      I2C_updateConfig(LOW, res); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("New calibration resolution is ");  Serial.println(res);
#endif
    return;
  }

  else if (opcode == 0x66) { // COMPARATOR UPSTREAM DETECT VOLTAGE OFFSET
    for (int i = 0; i <= 3; i++) {
      value = (uint8_t)bytesRecieved[i];
      if (checkNewConfig(value, 0, 127) == HIGH) {
        TOF[i].comparatorOffset_det[0] = (uint8_t)value;
      }
#ifdef I2CDebug
      Serial.print("Offset (UP DET) for Cha ");  Serial.print(i + 1);  Serial.print(" = ");  Serial.println(TOF[i].comparatorOffset_det[0]);
#endif
    }
    I2C_updateConfig(HIGH, TOF[0].comparatorOffset_det[0]); // MAX35 update required? Signed? Byte count? config variable
    return;
  }

  else if (opcode == 0x67) { // COMPARATOR DOWNSTREAM DETECT VOLTAGE OFFSET
    for (int i = 0; i <= 3; i++) {
      value = (uint8_t)bytesRecieved[i];
      if (checkNewConfig(value, 0, 127) == HIGH) {
        TOF[i].comparatorOffset_det[1] = (uint8_t)value;
      }
#ifdef I2CDebug
      Serial.print("Offset (DN DET) for Cha "); Serial.print(i + 1);  Serial.print(" = ");  Serial.println(TOF[i].comparatorOffset_det[1]);
#endif
    }
    I2C_updateConfig(HIGH, TOF[0].comparatorOffset_det[1]); // MAX35 update required? Signed? Byte count? config variable
    return;
  }

  else if (opcode == 0x68) { // COMPARATOR UPSTREAM RETURN VOLTAGE OFFSET
    for (int i = 0; i <= 3; i++) {
      value = (int8_t)bytesRecieved[i];
      if (checkNewConfig(value, -127, 127) == HIGH) {
        TOF[i].comparatorOffset_ret[0] = (int8_t)value;
      }
#ifdef I2CDebug
      Serial.print("Offset (UP RET) for Cha "); Serial.print(i + 1);  Serial.print(" = ");  Serial.println(TOF[i].comparatorOffset_ret[0]);
#endif
    }
    I2C_updateConfig(HIGH, TOF[0].comparatorOffset_ret[0]); // MAX35 update required? Signed? Byte count? config variable
    return;
  }

  else if (opcode == 0x69) { // COMPARATOR DOWNSTREAM RETURN VOLTAGE OFFSET
    for (int i = 0; i <= 3; i++) {
      value = (int8_t)bytesRecieved[i];
      if (checkNewConfig(value, -127, 127) == HIGH) {
        TOF[i].comparatorOffset_ret[1] = (int8_t)value;
      }
#ifdef I2CDebug
      Serial.print("Offset (DN RET) for Cha "); Serial.print(i + 1);  Serial.print(" = ");  Serial.println(TOF[i].comparatorOffset_ret[1]);
#endif
    }
    I2C_updateConfig(HIGH, TOF[0].comparatorOffset_ret[1]); // MAX35 update required? Signed? Byte count? config variable
    return;
  }

  else if (opcode == 0x70) { // MEASUREMENT DURATION
    value = combineBytes(0B00000011);
    if (checkNewConfig(value, LBaveTimeTOF, HBaveTimeTOF) == HIGH) {
      aveTimeTOF = (uint16_t)value;
      I2C_updateConfig(LOW, aveTimeTOF); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("New measurement duration is ");  Serial.print(aveTimeTOF);  Serial.println(" milliseconds.");
#endif
    return;
  }

  else if (opcode == 0x71) { // DETECT ON RISING (1) OR FALLING (0) EDGE
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBrising_falling, HBrising_falling) == HIGH) {
      rising_falling = (boolean)value;
      I2C_updateConfig(HIGH, rising_falling); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    if (rising_falling == 1)
      Serial.println("Now detecting upon FALLING edge");
    else
      Serial.println("Now detecting upon RISING edge");
#endif
    return;
  }

  else if (opcode == 0x72) { // TRANSMIT VOLTAGE. ATEX CURRENT LIMIT IS LVL7
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBpulseVoltage, HBpulseVoltage) == HIGH) {
      pulseVoltage = (uint8_t)value;
      I2C_updateConfig(HIGH, pulseVoltage); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Voltage is ");  Serial.println(pulseVoltage);
#endif
    return;
  }

  else if (opcode == 0x73) { // LAUNCH DELAY
    value = combineBytes(0B00000011);
    if (checkNewConfig(value, LBlaunch_delay, HBlaunch_delay) == HIGH) {
      launch_delay = (uint16_t)value;
      I2C_updateConfig(LOW, launch_delay); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Pulse launch delay is ");  Serial.print(launch_delay);  Serial.println(" microseconds.");
#endif
    return;
  }

  else if (opcode == 0x74) { // MEASUREMENT TIMEOUT LIMIT
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBtimeoutLimit, HBtimeoutLimit) == HIGH) {
      timeoutLimit = (uint8_t)value;
      I2C_updateConfig(LOW, timeoutLimit); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Measurement timeout limit is ");  Serial.print(timeoutLimit);  Serial.println(" milliseconds.");
#endif
    return;
  }

  else if (opcode == 0x80) { // AVERAGING METHOD
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBaveMethod, HBaveMethod) == HIGH) {
      aveMethod = (uint8_t)value;
      I2C_updateConfig(LOW, aveMethod); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    if (aveMethod == 0)
      Serial.println("Using arithmetic average");
    if (aveMethod == 1)
      Serial.println("Using harmonic average");
#endif
    return;
  }

  else if (opcode == 0x81) { // EXPECTED TOF
    value = combineBytes(0B00000011);
    if (checkNewConfig(value, LBexpectedTOF, HBexpectedTOF) == HIGH) {
      expectedTOF = (uint16_t)value;
      I2C_updateConfig(HIGH, expectedTOF); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Expected transit time is ");  Serial.print(expectedTOF);  Serial.println(" microseconds");
#endif
    return;
  }

  else if (opcode == 0x82) { // TOF RECIEVE WINDOW
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBrxWindow, HBrxWindow) == HIGH) {
      rxWindow = (uint8_t)value;
      I2C_updateConfig(HIGH, rxWindow); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("New recieve window is +/- ");  Serial.print(rxWindow);  Serial.println(" microseconds");
#endif
    return;
  }

  else if (opcode == 0x83) { // MAX ALLOWED TOF DIFFERENCE
    value = combineBytes(0B00000011);
    if (checkNewConfig(value, LBTOFlimit, HBTOFlimit) == HIGH) {
      TOFlimit = (uint16_t)value;
      I2C_updateConfig(LOW, TOFlimit); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Maximum TOF difference accepted is ");  Serial.print(TOFlimit);  Serial.println(" nanoseconds");
#endif
    return;
  }

  else if (opcode == 0x84) { // OUTLIER REMOVAL THRESHOLD
    value = combineBytes(0B00000011);
    if (checkNewConfig(value, LBSD_threshold, HBSD_threshold) == HIGH) {
      SD_threshold = (uint16_t)value;
      I2C_updateConfig(LOW, SD_threshold); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Outlier removal threshold now ");  Serial.println(SD_threshold);
#endif
    return;
  }

  else if (opcode == 0x85) { // SUCCESS RATIO CUTOFF
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBcha_cutoff, HBcha_cutoff) == HIGH) {
      cha_cutoff = (uint8_t)value;
      I2C_updateConfig(LOW, cha_cutoff); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Success ratio cutoff limit is ");  Serial.print(cha_cutoff); Serial.println(" %");
#endif
    return;
  }

  else if (opcode == 0x86) { // ENABLE/DISABLE TURBULENCE CORRECTION
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBcorrectFlow, HBcorrectFlow) == HIGH) {
      correctFlow = (boolean)value;
      I2C_updateConfig(LOW, correctFlow); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    if (correctFlow == 0)
      Serial.println("Turbulence correction is DISABLED!");
    if (correctFlow == 1)
      Serial.println("Turbulence correction is ENABLED.");
#endif
    return;
  }

  else if (opcode == 0x87) { // ENABLE/DISABLE WAVELENGTH SKIP ERROR CORRECTION
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBbErrorCorrect, HBbErrorCorrect) == HIGH) {
      bErrorCorrect = (boolean)value;
      I2C_updateConfig(LOW, bErrorCorrect); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    if (bErrorCorrect == 0)
      Serial.println("Wavelength skipping correction is DISABLED.");
    if (bErrorCorrect == 1)
      Serial.println("Wavelength skipping correction is ENABLED.");
#endif
    return;
  }

  else if (opcode == 0x90) { // fluid select
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBfluid , HBfluid ) == HIGH) {
      fluid  = (uint8_t)value;
      I2C_updateConfig(LOW, fluid); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Selected fluid is ");  Serial.println(fluid);
    switch (fluid) {
      case 0:
        Serial.print(" Water.");
        return;
      case 1:
        Serial.print(" Kerosene");
        return;
      case 2:
        Serial.print(" Aztec crude");
        return;
      case 3:
        Serial.print(" Methane");
        return;
      default: return;
    }
#endif
    return;
  }

  else if (opcode == 0x91) { // TRANSDUCER PULSE ANGLE IN TENTHS OF DEGREES FROM NORMAL
    value = combineBytes(0B00000011);
    if (checkNewConfig(value, LBtAngle, HBtAngle) == HIGH) {
      tAngle = (uint16_t)value;
      I2C_updateConfig(LOW, tAngle); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Transducer transmit angle is ");  Serial.print((float)tAngle / 10);  Serial.print(" degrees ");
#endif
    return;
  }

  else if (opcode == 0x92) { // TRANSDUCER SOUND SPEED
    value = combineBytes(0B00001111);
    if (checkNewConfig(value, LBSoSt, HBSoSt) == HIGH) {
      SoSt = (uint32_t)value;
    }
    I2C_updateConfig(LOW, SoSt); // MAX35 update required? Signed? Byte count? config variable
#ifdef I2CDebug
    Serial.print("Transducer sound-speed now ");  Serial.print((float)SoSt / 1000);  Serial.println(" m/s");
#endif
    return;
  }

  else if (opcode == 0x93) { // BOUNCE COUNT
    value = bytesRecieved[0];
    if (checkNewConfig(value, LBbounceCount, HBbounceCount) == HIGH) {
      bounceCount = (uint8_t)value;
      I2C_updateConfig(LOW, bounceCount); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Bounce count is now ");  Serial.println(bounceCount);
#endif
    return;
  }

  else if (opcode == 0x94) { // PIPE DIAMETER
    value = combineBytes(0B00000011);
    if (checkNewConfig(value, LBpDi, HBpDi) == HIGH) {
      pDi = (uint16_t)value;
      I2C_updateConfig(LOW, pDi); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Pipe diameter is now ");  Serial.print(pDi);  Serial.println(" mm");
#endif
    return;
  }

  else if (opcode == 0x95) {  // DISABLE TOF CHANNEL
    uint8_t cha = 0; // just for transducer enable because its boolean array
    USTenable[bytesRecieved[0] - 1] = LOW; // disable the specified pair
    setPowerModes();
    for (int i = 0; i < SC_TOF; i++) {
      if (USTenable[i] == HIGH)
        bitWrite(cha, i, HIGH);
      else
        bitWrite(cha, i, LOW);
    }
    I2C_updateConfig(LOW, cha); // MAX35 update required? Signed? Byte count? config variable
#ifdef I2CDebug
    Serial.print("Channel ");  Serial.print(bytesRecieved[0]);  Serial.println(" disabled");
#endif
    return;
  }

  else if (opcode == 0x96) {  // ENABLE TOF CHANNEL
    uint8_t cha = 0; // just for transducer enable because its boolean array
    USTenable[bytesRecieved[0] - 1] = HIGH; // enable the specified pair
    setPowerModes();
    for (int i = 0; i < SC_TOF; i++) {
      if (USTenable[i] == HIGH)
        bitWrite(cha, i, HIGH);
      else
        bitWrite(cha, i, LOW);
    }
    I2C_updateConfig(LOW, cha); // MAX35 update required? Signed? Byte count? config variable
#ifdef I2CDebug
    Serial.print("Channel ");  Serial.print(bytesRecieved[0]);  Serial.println(" enabled");
#endif
    return;
  }

  else if (opcode == 0x97) {  // BASELINE CHANNEL 1
    value = combineBytes(0B00001111);
    if (checkNewConfig(value, -500000, 500000) == HIGH) { // min=-500ns, max=500ns
      TOF[0].baseline = (int32_t)value;
      I2C_updateConfig(LOW, TOF[0].baseline); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Baseline for channel 1 now = ");  Serial.print(TOF[0].baseline);
#endif
    return;
  }

  else if (opcode == 0x98) {  // BASELINE CHANNEL 2
    value = combineBytes(0B00001111);
    if (checkNewConfig(value, -500000, 500000) == HIGH) { // min=-500ns, max=500ns
      TOF[1].baseline = (int32_t)value;
      I2C_updateConfig(LOW, TOF[1].baseline); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Baseline for channel 2 now = ");  Serial.print(TOF[1].baseline);
#endif
    return;
  }

  else if (opcode == 0x99) {  // BASELINE CHANNEL 3
    value = combineBytes(0B00001111);
    if (checkNewConfig(value, -500000, 500000) == HIGH) { // min=-500ns, max=500ns
      TOF[2].baseline = (int32_t)value;
      I2C_updateConfig(LOW, TOF[2].baseline); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Baseline for channel 3 now = ");  Serial.print(TOF[2].baseline);
#endif
    return;
  }

  else if (opcode == 0x9A) {  // BASELINE CHANNEL 1
    value = combineBytes(0B00001111);
    if (checkNewConfig(value, -500000, 500000) == HIGH) { // min=-500ns, max=500ns
      TOF[3].baseline = (int32_t)value;
      I2C_updateConfig(LOW, TOF[3].baseline); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Baseline for channel 4 now = ");  Serial.print(TOF[3].baseline);
#endif
    return;
  }

  else if (opcode == 0x9B) {  // DIFFERENTIAL DRIVE YES OR NO
    value = bytesRecieved[0];
    if (checkNewConfig(value, 0, 1) == HIGH) { // min=-500ns, max=500ns
      single_ended = (boolean)value;
      I2C_updateConfig(LOW, single_ended); // MAX35 update required? Signed? Byte count? config variable
    }
#ifdef I2CDebug
    Serial.print("Single-ended drive is now ");  Serial.print(single_ended);
#endif
    return;
  }

  else if (opcode == 0x9C) {  // FLOW RESCALER
    flow_scaler = (int32_t)combineBytes(0B00001111);
    //    if (checkNewConfig(value, -500000, 500000) == HIGH) { // min=-500ns, max=500ns
    //      //flow_scaler = (int32_t)value;
    I2C_updateConfig(LOW, flow_scaler); // MAX35 update required? Signed? Byte count? config variable
    //    }
#ifdef I2CDebug
    Serial.print("Flow scaler now = ");  Serial.print(flow_scaler);
#endif
    return;
  }

  else if (opcode == 0x9D) {  // FLOW OFFSET
    flow_offset  = (int32_t)combineBytes(0B00001111);
    //    if (checkNewConfig(value, -500000000, 500000000) == HIGH) { // in ul/s +/- 500L/s
    //      flow_offset = (int32_t)value;
    I2C_updateConfig(LOW, flow_offset); // MAX35 update required? Signed? Byte count? config variable
    //    }
#ifdef I2CDebug
    Serial.print("Flow offset now = ");  Serial.print(flow_offset);
#endif
    return;
  }

  else if (opcode == 0x9E) {  // FLOW OFFSET
    flow_cutoff = bytesRecieved[0];
    //    if (checkNewConfig(value, -500000000, 500000000) == HIGH) { // in ul/s +/- 500L/s
    //      flow_offset = (int32_t)value;
    I2C_updateConfig(LOW, flow_cutoff); // MAX35 update required? Signed? Byte count? config variable
    //    }
#ifdef I2CDebug
    Serial.print("Flow cut-off for accumulation = ");  Serial.print(flow_cutoff);
#endif
    return;
  }

  invalidOpcodeRecieved();
} //********************** END OF RECIEVE EXTERNAL OPCODE COMMAND *************************
