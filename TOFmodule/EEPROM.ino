// TODO
// use 64 bits
// only update EEPROM at long intervals. update constantly in ram.
// record epoc time of last update, so a total of 12 bytes per update
// on startup, check last updated volume and time. average first measurement with previous one, and caucltae average volume during downtime.

// identify previous address
// read accumulated flow
// calculate additional flow since last update
// add to accumulated flow
// update flow into next address.

// upon startup reads the accumulated flow from EEPROM as the initial value. Every address will be queried, with the largest value assumed to be the latest.
// accumulates the volumetric flow (in ml) after each measurement.
// Periodically saves the updated accumulated flow to EEPROM, using a different memory address each time.

// CRC
// include with volume block. calc CRC on its own local block, comparator calibration, and total timestamp.
void accumulate_flow() {
  uint8_t accRate = 60; // time in seconds between each update to EEPROM
  double   addVol = 0;  // additional volume added for each measurement.
  float   t = (float)(millis() - accTime) / 1000; // // seconds since last update in ram
  accTime = millis();  // used to calculate t above

#ifdef DOWNTIME_COMPENSATE
  // compensating for downtime while system is offline
  if (accVolContinue == HIGH) {
#ifdef eepromDebug
    Serial.println("!! CONTINUING FROM PREVIOUS ACCUMULATION LOG !!");
#endif
    if (downTime < 600) { // don't account for downtime if its greater than 10 minutes
      t += downTime;
    }
    downTime = 0;
    accVolContinue = LOW;
  }
#endif
  // how much total volume has flown since last update?
  if ((abs(combinedFlow)) < ((uint32_t)flow_cutoff * 1000000l)) { // don't accumulate on super-low (probably 0 real) flow.
    addVol = 0;
  }
  else{
    addVol = (double)combinedFlow / 1000000.0; // from ul/s to l/s. average flow in litres
    addVol = addVol * (double)t; // multiplied by number of seconds for total vol
  }
#ifdef coreDebug
  //Serial.print(t, 3); Serial.println(" seconds since last accumulation update");
  Serial.print(addVol, 2);  Serial.println(" litres has accumulated since last update");
#endif

  // update local memory
  if (accFlowU.volDouble + addVol > 4000000000) { // if the result exceeds 4 billion then rollover variable.
    accFlowU.volDouble = addVol;
  }
  else {
    accFlowU.volDouble = accFlowU.volDouble + addVol;
  }

  // checking to ensure accumulation is now non-zero
  if ((addVol =! 0) && (accFlowU.volDouble == 0)) {
      accFlowU.volDouble = 0;
      clearEEPROM();
      saveCalibration();
      EEPROM_initialisation();
    #ifdef errorDebug
    Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  VOLUME FAILED TO ACCUMULATE! EEPROM AND MEMORY ARE NOW RESET.  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    #endif
  }
  

#ifdef coreDebug
  Serial.print("Total accumulated volume = ");
  printDouble(accFlowU.volDouble, 1);
  Serial.println(" litres");
#endif

  // has enough time passed that a write to EEPROM is needed?
  if (Teensy3Clock.get() >= (accTimeU.timeInt + accRate)) {
#ifdef eepromDebug
    Serial.println("Updating EEPROM with accumulated flow...");
#endif

    accTimeU.timeInt = Teensy3Clock.get(); // update epoc time of latest EEPROM update

    accVolAddress += 13;  // increment address
    // time to wrap around memory locations? (1027-1039 is final volume address. 1044-1075 for calibration, 76, 77, 78, 79 reserved for start time)
    if (accVolAddress > 1027) {
      accVolAddress = 0;
    }

    // update the EEPROM
    for (int i = 0; i < 8; i++) {
      EEPROM.write((accVolAddress + i), accFlowU.volBytes[i]); // save acc vol
    }
    for (int i = 0; i < 4; i++) {
      EEPROM.write((accVolAddress + 8 + i), accTimeU.timeBytes[i]); // save timestamp
    }
    EEPROM.write((accVolAddress + 12), calcCRC(accVolAddress)); // save the CRC

  }
}


// address is the FIRST cell of the block of interest.
uint8_t calcCRC(uint16_t address) {
  uint8_t CRC = 0;

  // adding total volume...
  for (int i = 0; i < 8; i++) {
    CRC += EEPROM.read(address + i);
    //Serial.print("CRC = ");  Serial.println(CRC);
  }
  // adding timestamp...
  for (int i = 0; i < 4; i++) {
    CRC += EEPROM.read(address + 8 + i); // timestamp starts at cell 8
    //Serial.print("CRC = ");  Serial.println(CRC);
  }

  // adding baseline, calibration, and acc start time byte by byte...
  for (int a = 1044; a <= 1075; a++) {  // would go to 79 to include start time...
    CRC += EEPROM.read(a);
    //Serial.print("CRC = ");  Serial.println(CRC);
  }

  CRC = ~CRC; // flip the bits
#ifdef eepromDebug
  Serial.print("CRC calculated to be "); Serial.print(CRC);  Serial.print("  ");
#endif
  return CRC;
}


// checks CRC based on the values saved to EEPROM
// address is the FIRST cell of the block of interest.
boolean checkCRC(uint16_t address) {
  uint8_t CRC = EEPROM.read(address + 12); // read the CRC in specified epprom block
  Serial.print("CRC read at block ");  Serial.print(address / 13);  Serial.print(" = ");  Serial.print(CRC); Serial.print(".  ");


  if (calcCRC(address) != CRC) {
    Serial.print("!!!! CRC check FAILURE at EEPROM block ");  Serial.print(address / 13);  Serial.println(" !!!!");
    return LOW;
  }
  else {
    Serial.print("     CRC check SUCCESS at EEPROM block ");  Serial.println(address / 13);
    return HIGH;
  }
}


// runs on startup. Identifies most recently updated address containing accumulated flow.
void EEPROM_initialisation() {
  uint32_t timeStamp = 0;
  boolean calibrationOK = LOW;  // goes HIGH if a single CRC check passes, indicating the saved calibration is not corrupted.

  // timestamp is used to find the most recently updated memory block
  // 1080 memory locations, (1027-1039 is final volume address. 1044-1075 for calibration, 76, 77, 78, 79 reserved for start time)
  for (int a = 0; a <= 1027; a += 13) { // first 4 bytes for timestamp, next 8 for acc vol, 1 for CRC = 13.
    for (int i = 0; i < 8; i++) {
      accFlowU.volBytes[i] = EEPROM.read(a + i); // read volume
    }
    //Serial.print("Address block ");  Serial.print(a / 13); Serial.print("   timestamp = ");  Serial.print(accTimeU.timeInt);

    for (int i = 0; i < 4; i++) {
      accTimeU.timeBytes[i] = EEPROM.read(a + 8 + i); // read timestamp
    }
    //Serial.print("   Acc-Vol = "); Serial.println(accFlowU.volDouble);

    // if the most recently read timestamp is greater than previous highest,
    // AND it passes the CRC check...   then update highest and save address.
    if (checkCRC(a) == HIGH) {
      calibrationOK = HIGH;  // if any CRC check passes this means the calibration/baseline was not corrupted.
      if (accTimeU.timeInt > timeStamp)  {
        timeStamp = accTimeU.timeInt;
        accVolAddress = a;
      }
    }
  }  // iterate to next memory block

  // update the accumulated flow value from EEOPROMs latest address
  for (int i = 0; i < 8; i++) { // read acc volume from identified most recent address
    accFlowU.volBytes[i] = EEPROM.read(accVolAddress + i);
    //Serial.print("   Address ");  Serial.print(a + i); Serial.print(" holds "); Serial.print(accFlowU.volBytes[i]);
  }
  Serial.print("Latest timestamp = ");  Serial.println(timeStamp);
  Serial.print("Latest accumulated volume = "); Serial.println(accFlowU.volDouble);


  // if all timestamps are zero, then it's time to start afresh. Record the start time (seconds since 1970).
  // this will also trigger if every CRC check fails.
  if (timeStamp == 0 || calibrationOK == LOW) {
    if (calibrationOK == LOW) {
      Serial.println("!!!! Every CRC check failed. EEPROM corrupted. Clearing EEPROM and applying defaults !!!!");
      defaultCalibration();
      clearEEPROM(); // set every cell to 0. all CRC cells to 255
      saveCalibration(); // also updates CRC values.
    }
    Serial.println("No accumulated timestamp on record. Starting afresh");

    accVolContinue = LOW; // nothing to continue from, so don't account for historical vol.
    accVolAddress = 0;
    accTimeU.timeInt = Teensy3Clock.get();
    Serial.print("Epoc time is ");  Serial.println(accTimeU.timeInt);
    // Writing start time to reserved address
    for (int i = 0; i < 4; i++) {
      EEPROM.write((1076 + i), accTimeU.timeBytes[i]);
    }
  }
  else {
    for (int i = 0; i < 4; i++) {
      accTimeU.timeBytes[i] = EEPROM.read(1076 + i);
    }
    accTimeStart = accTimeU.timeInt;
    Serial.print("Volume accumulation started at  ");  Serial.print(accTimeStart);  Serial.println(" Epoc time. ");
    Serial.print("Latest volume update occured at ");  Serial.println(timeStamp);
    downTime = Teensy3Clock.get() - timeStamp;
    Serial.print("Resulting in a downtime of ");  Serial.print(downTime);  Serial.println(" seconds. ");
  }

}


void saveCalibration() {

  // save baseline
  for (int p = 0; p < 4; p++) {
    baselineU.baselineInt = TOF[p].baseline;
    Serial.print("Saving baseline "); Serial.println(baselineU.baselineInt);

    for (int i = 0; i < 4; i++) {
      EEPROM.write((1044 + (p * 4) + i), baselineU.baselineBytes[i]); // save acc vol
    }
  }

  // save calibration
  int p = 0;
  for (int a = 1060; a <= 1072; a += 4) { // memory locations 1060 to 1075 adding comparator calibration...
    EEPROM.write((a + 0), TOF[p].comparatorOffset_det[0]);
    Serial.print(TOF[p].comparatorOffset_det[0]);  Serial.print(" saved (det-UP) to cell ");  Serial.println(a + 0);

    EEPROM.write((a + 1), TOF[p].comparatorOffset_det[1]);
    Serial.print(TOF[p].comparatorOffset_det[1]);  Serial.print(" saved (det-DN) to cell ");  Serial.println(a + 1);

    EEPROM.write((a + 2), TOF[p].comparatorOffset_ret[0]);
    Serial.print(TOF[p].comparatorOffset_ret[0]);  Serial.print(" saved (ret-UP)to cell ");  Serial.println(a + 2);

    EEPROM.write((a + 3), TOF[p].comparatorOffset_ret[1]);
    Serial.print(TOF[p].comparatorOffset_ret[1]);  Serial.print(" saved (ret-DN) to cell ");  Serial.println(a + 3);
    p++;
  }

  // Updates all the CRC values now that some of the shared cells (calibration etc) have changed
  //uint8_t CRC = 0;
  for (int a = 0; a <= 1027; a += 13) {
    EEPROM.write((a + 12), calcCRC(a)); // calc CRC expects address of first cell of each block
  }
  Serial.println();
}


// reads in the stored comparator calibration values
void loadCalibrationEEPROM() {
  int p = 0;
  for (int a = 1044; a < 1060; a += 4) { // memory locations 1060 to 1075 adding comparator calibration...
    for (int i = 0; i < 4; i++) {
      baselineU.baselineBytes[i] = EEPROM.read(a + i);
    }
    TOF[p].baseline = baselineU.baselineInt;
    Serial.print("Loading baseline... ");  Serial.print(TOF[p].baseline);  Serial.print(" ps read from address ");  Serial.println(a);
    p++;
  }

  Serial.println("Loading comparator calibrations...");
  p = 0;
  for (int a = 1060; a < 1073; a += 4) { // memory locations 1060 to 1075 adding comparator calibration...
    TOF[p].comparatorOffset_det[0] = EEPROM.read(a + 0);
    Serial.print("Cha");  Serial.print(p + 1);  Serial.print(" DET-UP from cell ");  Serial.print(a + 0);  Serial.print(" = ");  Serial.println(TOF[p].comparatorOffset_det[0]);

    TOF[p].comparatorOffset_det[1] = EEPROM.read(a + 1);
    Serial.print("Cha");  Serial.print(p + 1);  Serial.print(" DET-DN from cell ");  Serial.print(a + 1);  Serial.print(" = ");  Serial.println(TOF[p].comparatorOffset_det[1]);

    TOF[p].comparatorOffset_ret[0] = EEPROM.read(a + 2);
    Serial.print("Cha");  Serial.print(p + 1);  Serial.print(" RET-UP from cell ");  Serial.print(a + 2);  Serial.print(" = ");  Serial.println(TOF[p].comparatorOffset_ret[0]);

    TOF[p].comparatorOffset_ret[1] = EEPROM.read(a + 3);
    Serial.print("Cha");  Serial.print(p + 1);  Serial.print(" RET-UP from cell ");  Serial.print(a + 3);  Serial.print(" = ");  Serial.println(TOF[p].comparatorOffset_ret[1]);

    p++;
  }
  generate_MAXconfig();
  upload_MAXconfig(-1);
}


void printEEPROM() {
  accFlowUnion accFlow;
  accTimeUnion accTime;
  baselineUnion baseline;

  for (int a = 0; a <= 1027; a += 13) { // first 4 bytes for timestamp, next 8 for acc vol, 1 for CRC = 13.
    Serial.print("Address block ");  Serial.print(a / 13);
    for (int i = 0; i < 4; i++) {
      accTime.timeBytes[i] = EEPROM.read(a + i); // read timestamp
      //Serial.print(" cell ");  Serial.print(a + i);  Serial.print(" = ");  Serial.print(accTimeU.timeBytes[i]);
    }
    for (int i = 0; i < 8; i++) {
      accFlow.volBytes[i] = EEPROM.read(a + 4 + i); // read vol
      //Serial.print(" cell ");  Serial.print(a + i);  Serial.print(" = ");  Serial.print(accFlowU.volBytes[i]);
    }
    Serial.print("   timestamp = ");  Serial.print(accTime.timeInt);
    Serial.print("   Vol total = ");  Serial.print(accFlow.volDouble);
    Serial.print("   CRC = ");        Serial.println(EEPROM.read(a + 12));
  }

  // baseline
  Serial.print("Baselines for all channels in ns");
  for (int p = 1044; p <= 1059; p += 4) {
    baseline.baselineBytes[0] = EEPROM.read(p + 0);
    baseline.baselineBytes[1] = EEPROM.read(p + 1);
    baseline.baselineBytes[2] = EEPROM.read(p + 2);
    baseline.baselineBytes[3] = EEPROM.read(p + 3);
    Serial.print(", "); Serial.print(baseline.baselineInt);
  }
  Serial.println();

  // comparator calibration
  for (int p = 1060; p <= 1072; p += 4) {
    Serial.print("det UP = "); Serial.print(EEPROM.read(p + 0));
    Serial.print(". det DN = "); Serial.print(EEPROM.read(p + 1));
    Serial.print(". ret UP = "); Serial.print(EEPROM.read(p + 2));
    Serial.print(". ret DN = "); Serial.println(EEPROM.read(p + 3));
  }

  // read start-timestamp
  for (int i = 0; i < 4; i++) {
    accTime.timeBytes[i] = EEPROM.read(1076 + i);
  }
  Serial.print("Totalisation start-time = ");  Serial.println(accTime.timeInt);
}


void clearEEPROM() {
  for (int a = 0; a < 1080; a++) {
    EEPROM.write(a, 0);
  }
}
