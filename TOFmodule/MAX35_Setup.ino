// THIS SET OF FUNCTIONS CONFIGUres THE MAX35 CHIPS


boolean MAXSetup() {// ********* Configure MAX35 *********
  wdt.feed();  // ensure this has plenty of time to complete.
  for (uint8_t p = 0; p < SC_TOF; p++) {
    pinMode(pAF_EN[p], OUTPUT); // enable amplifier/filter circuits
    pinMode(pCS_MAX[p], OUTPUT);  // chip select is output
    digitalWrite(pCS_MAX[p], HIGH); // Ensure all SPI chip selects are inactive
    pinMode(pINT_MAX[p], INPUT_PULLUP); // pullup, because interrupts are on falling edge
    digitalWrite(11, LOW); // force MOSI low to reset
    digitalWrite(12, LOW); // force MISO low to reset
    digitalWrite(13, LOW); // SCK MOSI low to reset    
    intReg_MAX[p] = 0; // ensuring the interrupt register starts out clean and clear
  }
  attachInterrupt(pINT_MAX[0], interruptMAX0, FALLING);
  attachInterrupt(pINT_MAX[1], interruptMAX1, FALLING);
  attachInterrupt(pINT_MAX[2], interruptMAX2, FALLING);
  attachInterrupt(pINT_MAX[3], interruptMAX3, FALLING);
  
  delay(5);
  pinMode(SCK, OUTPUT);
  SPI.begin(); delay(10);

  generate_MAXconfig(); // configures an array (for each UST channel) to hold default MAX config ready for upload.
  for (uint8_t p = 0; p < SC_TOF; p++)
    readFromMAX(p, 0xFE); // read int registers to clear them

  delay(10);
  setPowerModes(); // turns on/off filter/amps for transducer pairs
  testMAXSPI(); // tests to see if read and write via SPI are both working.


  Serial.println();
  resetMAX(HIGH); // manages setup and config of MAX35 chips

  if (validate_MAXconfig() == HIGH) {
    Serial.print("Failed to configure and validate MAX35 chips! Attempting again...");
    return LOW;
  }
  else {
    return HIGH;
  }
}


boolean testMAXSPI() { // writes to the delay register, and reads back to check SPI.
  uint16_t delayRegBackup = readFromMAX(0, 0xC1); delay(10);; // remember the register value
  uint16_t delayReg = 0;
  boolean error = LOW;
  for (int p = 0; p < SC_TOF; p++) { // iterate over TOF transducer pairs
    delayRegBackup = readFromMAX(p, 0xC1); delay(10); // remember the register value
    delay(50);
    writeToMAX(p, 0x41, 0B0000000000000000, LOW);  delay(10); // clear register
    writeToMAX(p, 0x41, 0B1010101010101010, LOW);  delay(10); // write an unusual value
    Serial.print("SPI with MAX35104 pair ");  Serial.print(p + 1);  Serial.print("... ");


    delayReg = readFromMAX(p, 0xC1);
    Serial.print("Register reads ");  Serial.print(delayReg, BIN);


    if (delayReg == 0B1010101010101010) {  // check unusual value
      Serial.print(". Channel ");  Serial.print(p + 1);  Serial.println(" SPI Functional");
    }
    else {
      delay(10);
      writeToMAX(p, 0x41, 0B1010101010101010, LOW);  // if fail, try again.
      delay(10);
      delayReg = readFromMAX(p, 0xC1);
      Serial.print(". 1st try failed. 2nd attemp reads ");  Serial.print(delayReg, BIN);
      if (delayReg != 0B1010101010101010) { // 2nd fail results in error
        Serial.print(". !! Channel ");  Serial.print(p + 1);  Serial.println(" SPI ERROR !!");
        error =  HIGH; // error
      }
      else {
        Serial.print(". Channel ");
        Serial.print(p + 1);
        Serial.println(" SPI Functional");
      }
    }

    writeToMAX(p, 0x41, delayRegBackup, LOW);  delay(10); // restore the register value
  } // iterate over transducer pairs
  return error;
}

void generate_MAXconfig() { // loads MAX35 configuration into an array ready for upload
  for (uint8_t p = 0; p < SC_TOF ; p++) {
    // do all channels, even if some are currently disabled

    // SWITCHER 1 REGISTER
    //       default 0000000000110000
    TOF[p].MAXconfig[0] = 0B1000000010111111;
    for (uint8_t b = 0; b <= 3; b++)
      bitWrite( TOF[p].MAXconfig[0], b, bitRead(pulseVoltage + 3, b)); // setting output voltage level
    // 10     [15:14] Switching frequency of switcher boost circuit 00=100, 01=125, 10=166, 11=200 KHz. SET to 166KHz.
    // 0         [13] disable high voltage regulator? 1 is disabled.
    // 0         [12] MUST always be written 0.
    // 0000    [11:8] reserved.
    // 10       [7:6] Switching frequency of doubler circuit 00=100, 01=125, 10=166, 11=200 KHz.
    // 11       [5:4] MUST always be written 1.
    // 1111     [3:0] Target output voltage. starts at 0100=9, 0101=10.8, 0110=12.6, 0111=15, 1000=16.8, 1001=19.2, 1010=21, 22.8, 25.2, 27, 28.8, 30.6V

    // SWITCHER 2 REGISTER
    //       default 0100010011100000
    TOF[p].MAXconfig[1] = 0B0000000010011000;
    // 0000   [15:12] max inductor current in normal duty mode. 0000= loop conditions determine max.
    // 0000    [11:8] max inductor current during power-up 1000 is 1.6v/RSENSE=max, highest except for no limit (0000).
    // 1001     [7:4] switcher stabilization time 0000=64us, up to 16.4ms. 1001=1.02ms.
    // 1          [3] setting to 1 allows FET to exceed a 50% max dutry cycle based on target output voltage (SWITCHER1 [3:0])
    // 00       [2:1] MUST always be written 0.
    // 0          [0] Enable pulse echo mode? Transmitter is also reciever. 0 is normal TOF mode.

    // AFE1 REGISTER
    //              default 0000010000011001
    TOF[p].MAXconfig[2] = 0B1000000000010111;
    bitWrite(TOF[p].MAXconfig[2], 10, single_ended);
    uint16_t AFE = readFromMAX(0, 0x96); // MAX pair 0 will be used as template config for all
    for (uint8_t b = 0; b <= 6; b++) // read and write back bits 0 to 6.
      bitWrite(TOF[p].MAXconfig[2], b, bitRead(AFE, b));
    // 1         [15] AFE bypass. When set to 1 both gain stages and BPF are removed from return signal-chain path.
    // 0000   [14:11] MUST always be written 0.
    // 1         [10] Single-ended drive enable. when set to 0 the transmitted square wave will be differential.
    // 00       [9:8] Route various AFE signals to CIP/CIN pin. 00 is disabled. Currently output variable gain stage.
    // 0          [7] MUST always be written 0.
    // 0010111  [6:0] NEVER CHANGE! Must be read and restored upon write.

    // AFE2 REGISTER.
    //       default 0000000000000000
    //      max gain 0000000011110001
    TOF[p].MAXconfig[3] = 0B0000000000110001;
    // 0         [15] 4 MHz bypass. when set the internal 4MHz oscillator is bypassed, and external 4MHz can be applied to 4MX1 pin.
    // 0000000 [14:8] Bandpass calibration. These bits are set by calling OPCODE command 06h.
    // 1100     [7:4] Programmable amplifier gain. 0 is gain of 3.16. F is gain of 31.44. Set to gain of
    // 00       [3:2] BPF Qfactor reducer. 00=12, 01=7.4, 10=5.3, 11=4.2 Q(Hz/Hz)
    // 0          [1] MUST always be written 0.
    // 1          [0] BPF bypass. removes BPF from return signal-chain path.
    //writeToMAX(p, 0x06, 0, HIGH);  // CALIBRATE BPF COMMAND
    //delay(500);

    // TOF1 REGISTER
    //  old setting  0111111100010001
    // 12 pulses            0000110000010001
    // 18 pulses            0001001000010000 // current
    // 20 pulses            0001010000010001
    // 26 pulses            0001101000010001
    // 30 pulses            0001111000010001
    // 36 pulses            0010010000010001
    // new setting          0100000000010001
    //                      ppppppppffffexxx
    TOF[p].MAXconfig[4] = 0B0001001000010000;
    // for (int b = 0; b < 8; b++)
    //  bitWrite(TOF[p].MAXconfig[4], b + 8, bitRead(pulseLength, b));
    //bitWrite(TOF[p].MAXconfig[4], 3, rising_falling);
    // 00100100 [15:8] pulse launcher size (number of pulses). 10000000 = 127 and is maximum.
    // 0001     [7:4] 0001 is pulse freq at 1 MHz. 1001 is 200KHz  Freq = 2MHz/(1+value).
    // 0          [3] Stop polarity for TDC. 0 is rising edge, 1 is falling edge.
    // 000      [2:0] reserved.


    // TOF2 REGISTER
    // default              1100001010110110
    // T2 is 6,             1110001101110100 // current
    // T2 is 10,            1110001011110100
    // T2 is 12             1110011001110100
    //                      hhhttttttdddxmmm
    TOF[p].MAXconfig[5] = 0B1110001100010100;
    // 111    [15:13] Stop hits. Set to use all 6. 000=1, 001=2, 010=3 etc.
    // 000110  [12:7] T2 wave selection (T2WV). Usually emperically determined. Currently set to 6.
    // 001      [6:4] TOF duty cycle. Delay between TOFUP and TOFDN measurements. 000=0, 001=122us, 010=244, 011=488, 100=732, 101=976us, 110=16.65ms, 111=19.97ms.
    // 0          [3] reserved
    // 100      [2:0] timeout for TOF measurements, in us. 000=128us, 001=256, 010=512, 011=1024, 100=2048, 101=4096, 110=8192, 111=16384.

    // TOF3 REGISTER
    // T2WV+1=7  HIT1+1=8   0000011100001000 // current
    // T2WV+1=11 HIT+1=12   0000101100001100
    // T2WV+2=14 HIT+2=16   0000111000010000
    //                      xxhhhhhhxxhhhhhh
    TOF[p].MAXconfig[6] = 0B0000011100001000;
    // 00     [15:14] reserved
    // 000111  [13:8] HIT1 wave select. At least 1 greater than T2WV. Set T2WV+1=7
    // 00       [7:6] reserved
    // 001000   [5:0] HIT2 wave select. At least 1 greater than HIT1. Set HIT1+1=8

    // TOF4 REGISTER
    // T2WV+1=9  HIT+1=10   0000100100001010 // current
    // T2WV+1=13 HIT+1=14   0000110100001110
    // T2WV+2=18 HIT+2=20   0001001000010100
    //                      xxhhhhhhxxhhhhhh
    TOF[p].MAXconfig[7] = 0B0000100100001010;
    // 00     [15:14] reserved
    // 010010  [13:8] HIT3 wave select. At least 1 greater than HIT2. Set HIT2+1=9
    // 00       [7:6] reserved
    // 010100   [5:0] HIT4 wave select. At least 1 greater than HIT3. Set HIT3+1=10

    // TOF5 REGISTER
    // T2WV+1=11  HIT+1=12  0000101100001100 // current
    // T2WV+1=15 HIT+1=16   0000111100010000
    // HIT4+2=22 HIT5+2=24  0001011000011000
    //                      xxhhhhhhxxhhhhhh
    TOF[p].MAXconfig[8] = 0B0000101100001100;
    // 00     [15:14] reserved
    // 010110  [13:8] HIT5 wave select. At least 1 greater than HIT4. Set HIT4+2=11
    // 00       [7:6] reserved
    // 011000   [5:0] HIT6 wave select. At least 1 greater than HIT5. Set HIT5+2=12

    // TOF6 REGISTER
    //        default 0000000000000000
    //                rrrrrrrrxccccccc
    //  baseline = +127 0000000001111111
    //   baseline = +80 0000000001010000
    //   baseline = +50 0000000000110010
    //   baseline = +40 0000000000101000
    //   baseline = +30 0000000000011110
    //   baseline = +20 0000000000010100
    //   baseline = +10 0000000000001010
    for (uint8_t b = 0; b <= 6; b++)
      bitWrite(TOF[p].MAXconfig[9], b, bitRead(TOF[p].comparatorOffset_det[0], b));
    for (uint8_t b = 8; b <= 15; b++)
      bitWrite(TOF[p].MAXconfig[9], b, bitRead(TOF[p].comparatorOffset_ret[0], b));
    bitWrite(TOF[p].MAXconfig[9], 7, 0); // bit 7 is reserved, ensure it is still 0.
    // 00000000 [15:8] comparator return baseline upstream from F7 to 01 = 127 to 1, 0=0, 80 to FF = -128 to -1. Currently set to 0.
    // X           [7] reserved
    // 0000000   [6:0] comparator baseline upstream 00 to 7F equal 0 to 127.


    // TOF7 REGISTER
    //        default 0000000000000000
    //                rrrrrrrrxccccccc
    //  baseline = +127 0000000001111111
    //   baseline = +80 0000000001010000
    //   baseline = +50 0000000000110010
    //   baseline = +40 0000000000101000
    //   baseline = +30 0000000000011110
    //   baseline = +20 0000000000010100
    //   baseline = +10 0000000000001010
    for (uint8_t b = 0; b <= 6; b++)
      bitWrite(TOF[p].MAXconfig[10], b, bitRead(TOF[p].comparatorOffset_det[1], b));
    for (uint8_t b = 8; b <= 15; b++)
      bitWrite(TOF[p].MAXconfig[10], b, bitRead(TOF[p].comparatorOffset_ret[1], b));
    bitWrite(TOF[p].MAXconfig[10], 7, 0); // bit 7 is reserved, ensure it is still 0.
    // 00000000 [15:8] comparator return baseline downstream from F7 to 01 = 127 to 1, 0=0, 80 to FF = -128 to -1. Currently set to 0.
    // X           [7] reserved
    // 0000000   [6:0] comparator baseline downstream 00 to 7F equal 0 to 127.

    // MEASUREMENT DELAY REGISTER
    //    default/min 0000000001100100
    uint16_t MAX_delay = 4 * (expectedTOF-rxWindow);
    TOF[p].MAXconfig[11] = MAX_delay;
    //MAXconfig[11] = 0B0000010101111000;
    // delay is multiple of 250 ns. Minimum value is 0064h (100*250=25us), up to FFFF.

    // CALIBRATION AND CONTROL REGISTER
    //        default 0000000000000000
    TOF[p].MAXconfig[12] = 0B0000001000000101;
    // 0000   [15:12] reserved
    // 0         [11] comparator output UP_DN enable
    // 0         [10] function select for CMP_OUT/UP_DN pin. disabled if [11] is 0
    // 1          [9] interupt pin enable
    // 0          [8] continue event timing mode continuously when set
    // 0          [7] when set causes the INT pin to assert on every event timing measurement finish
    // 101      [6:4] time to stabalise 4MHz clock before measurement. 100=5.13ms. 101=always on
    // 0000     [3:0] number of 32KHz time periods used to calibrate for 4MHz ceramic oscillator. N/A
  }
}


//******************* RESETTING AND RECONFIGURING THE MAX35 **********************
//********************************************************************************
void resetMAX(boolean por) { // This resets the MAX and reconfigures the config registers
  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers

    Serial.println();  Serial.print("Previous configuration, Channel ");  Serial.print(p + 1);
    printAndTest_MAXconfig(p);  Serial.println(); // Serial.prints all the config registers in binary for the specified transducer pair (p)

    if (por == HIGH) {
      Serial.println("Performing MAX35 reset command");
      writeToMAX(p, 0x04, 0, HIGH);  // MAX35 resET COMMAND
      delay(50);
    }

    Serial.print("Initialising.... ");
    writeToMAX(p, 0x05, 0, HIGH);  // INITIALISE COMMAND
    delay(50);
    Serial.println("Complete.");

    if (bitRead(TOF[0].MAXconfig[3], 0) == 0) {
      Serial.print("Calibrating Bandpass Filter.... ");
      writeToMAX(p, 0x06, 0, HIGH);  // CALIBRATE BPF
      delay(50);
      Serial.println("Complete.");
    }
  }  // iterate TOF pairs

  upload_MAXconfig(-1); // UPLOADING REGISTER CONFIGURATION TO ALL CHANNELS
  Serial.println("Uploaded master configuration to local registers");

  for (int p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers
    printAndTest_MAXconfig(p);  Serial.println(); // Serial.prints all the config registers in binary
  }  // iterate TOF pairs

}   //***************** END resETTING AND RECONFIGURING THE MAX35 ******************

// to upload to all pairs then pair_specify should  be negative
void upload_MAXconfig(int8_t pair_specify) { // uploads configuration registers to all active MAX channels
  uint8_t wait = 10;
  Serial.println();

  for (uint8_t p = 0; p < SC_TOF ; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers
    if ((pair_specify >= 0)  &&  (p =! pair_specify)) continue;  // if a pair is specified, then skip all except that
    #ifdef GENDebug
    Serial.print("Uploading configuration to MAX channel ");  Serial.print(p + 1); Serial.print("... ");
    #endif
    writeToMAX(p, 0x14, TOF[p].MAXconfig[0], LOW);  delay(wait);
    writeToMAX(p, 0x15, TOF[p].MAXconfig[1], LOW);  delay(wait);
    writeToMAX(p, 0x16, TOF[p].MAXconfig[2], LOW);  delay(wait);
    writeToMAX(p, 0x17, TOF[p].MAXconfig[3], LOW);  delay(wait);
    writeToMAX(p, 0x38, TOF[p].MAXconfig[4], LOW);  delay(wait);
    writeToMAX(p, 0x39, TOF[p].MAXconfig[5], LOW);  delay(wait);
    writeToMAX(p, 0x3A, TOF[p].MAXconfig[6], LOW);  delay(wait);
    writeToMAX(p, 0x3B, TOF[p].MAXconfig[7], LOW);  delay(wait);
    writeToMAX(p, 0x3C, TOF[p].MAXconfig[8], LOW);  delay(wait);
    writeToMAX(p, 0x3D, TOF[p].MAXconfig[9], LOW);  delay(wait);
    writeToMAX(p, 0x3E, TOF[p].MAXconfig[10], LOW);  delay(wait);
    writeToMAX(p, 0x41, TOF[p].MAXconfig[11], LOW);  delay(wait);
    writeToMAX(p, 0x42, TOF[p].MAXconfig[12], LOW);  delay(wait);

    // below registers are not used. as precaution they are all set to their POR values.
    writeToMAX(p, 0x30, 0B0000000000000000, LOW);  delay(wait); // RTC seconds
    writeToMAX(p, 0x31, 0B0000000000000000, LOW);  delay(wait); // RTC mins_hrs
    writeToMAX(p, 0x32, 0B0000000000000000, LOW);  delay(wait); // RTC day_date
    writeToMAX(p, 0x33, 0B0000000000000000, LOW);  delay(wait); // RTC yr_month
    writeToMAX(p, 0x34, 0B0000000000000000, LOW);  delay(wait); // watchdog alarm
    writeToMAX(p, 0x35, 0B0000000000000000, LOW);  delay(wait); // alarm
    writeToMAX(p, 0x3F, 0B0000000000000000, LOW);  delay(wait); // event timing 1
    writeToMAX(p, 0x40, 0B0000000000000000, LOW);  delay(wait); // event timing 2
    writeToMAX(p, 0x43, 0B0000000000000000, LOW);  delay(wait); // RTC
    //writeToMAX(p, 0xFF, 0B0000000000000000, LOW); // control reg. Don't write, contains hardware code.
    
    #ifdef GENDebug
    Serial.print("Done uploading for channel ");  Serial.println(p + 1);
    #endif
  }
}


void printAndTest_MAXconfig(uint8_t p) { // used by resetMAX
  const uint8_t MAXaddressesRead[13] = {0x94, 0x95, 0x96, 0x97, 0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 0xBE, 0xC1, 0xC2};
  uint16_t reg = 0;
  char *regName[] = {" SWITCHER 1: ", " SWITCHER 2: ", " AFE 1 reg:  ", " AFE 2 reg:  ", " TOF 1 reg:  ", " TOF 2 reg:  ",
                     " TOF 3 reg:  ", " TOF 4 reg:  ", " TOF 5 reg:  ", " TOF 6 reg:  ", " TOF 7 reg:  ", " DELAY reg:  ", " CAL & CONT: "
                    };

  for (uint8_t r = 0; r < 13; r++) {
    Serial.println();  Serial.print("Ch ");  Serial.print(p + 1);  Serial.print(regName[r]);
    reg = readFromMAX(p, MAXaddressesRead[r]);
    printBIN16(reg);
    if (validate_MAXregister(p, reg, r) == LOW)
      Serial.print(" OK. ");
    reg = 0; // clear just in case
  }
}


// Prints 16 bit binary out, including all zeros
void printBIN16(uint16_t b) { // used by printMAXconfig and others
  for (int i = 15; i >= 0; i--)
    Serial.print(bitRead(b, i));
}


boolean validate_MAXregister(uint8_t p, uint16_t reg, uint8_t r) { // pair number, register value, register number
  boolean alarm = LOW;
  boolean alert = LOW;

  delay(5);
  for (int b = 0; b < 16; b++) { // step through bits
    if ( bitRead(TOF[p].MAXconfig[r], b) != bitRead(reg, b) ) {
      if (alert == LOW) // if this register has already flagged an issue, just say which bit
        Serial.print(" Mismatch bit(s): ");
      Serial.print(b);  Serial.print(", ");
      alert = HIGH;
      alarm = HIGH;
    }
  } // iterate over bits
  return alarm;
}

boolean validate_MAXconfig() {
  boolean alarm = LOW;
  boolean alert = LOW;

  Serial.println();
  for (uint8_t p = 0; p < SC_TOF; p++) {
    if (USTenable[p] == LOW) continue; // ignore any disabled transducers

    alert = LOW;
    Serial.print("Validating register configuration - transducer channel ");  Serial.print(p + 1);  Serial.print("... ");

    // below holds the register addresses for the MAX35104 ICs
    const uint8_t MAXaddressesRead[13] = {0x94, 0x95, 0x96, 0x97, 0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 0xBE, 0xC1, 0xC2};
    for (int r = 0; r < 13; r++) { // step through registers
      delay(10);
      uint16_t reg = readFromMAX(p, MAXaddressesRead[r]);

      for (int b = 0; b < 16; b++) { // step through bits
        if ( bitRead(TOF[p].MAXconfig[r], b) != bitRead(reg, b) ) {
          alarm = HIGH;
          alert = HIGH;
        }
      }
    }
    if (alert == LOW)
      Serial.println(" OK.");
    else
      Serial.println(" Issue.");
  }
  if (alarm == LOW)
    Serial.println("All channels successfully validated.");
  return alarm;

}


void setPowerModes() {
  Serial.println();
  activePairs = 0;
  for (int p = 0; p < SC_TOF; p++)
    if (USTenable[p] == HIGH) {
      digitalWrite(pAF_EN[p], HIGH);
      Serial.print("Transducer channel ");  Serial.print(p + 1);  Serial.println(" Analogue power enabled.");
      activePairs++;
    }
    else {
      digitalWrite(pAF_EN[p], LOW);
      Serial.print("Transducer channel ");  Serial.print(p + 1);  Serial.println(" Analogue power disabled.");
    }
    Serial.print(activePairs);  Serial.println(" transducer channels are active");
}
