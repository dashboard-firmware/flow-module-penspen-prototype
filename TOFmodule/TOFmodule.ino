// Firmware for the Dashboard Time-of-Flight flow measurement module.
// Written by Liam Trimby - Version 1.0 - 04/01/2022.
// This code manages MAX35104 system-on-chips to make time-of-flight difference measurements using four pairs of ultrasonic transducers.
// Interfacing with this system is done via I2C, see associated documentation.
// Written in C++ to run on MIMXRT1062DVLXX processors.
// This code is entierly the property of Dashboard Limited and protected under copyright.

//TODO check for I2C transfer or read success etc
// use epprom to accumulate flow NOT for penspen.
// ensure all status bits have a set and unset condition.
// validate max35 registers after a config update
// It's possible for a valid opcode command to fail if it's sent with multiple bytes
// calibration should seek to find the correct up and downtime before checking SD and success of TOFdiff

#include <SPI.h>
#include "Configs.h"
#include <EEPROM.h>
#include <i2c_driver.h>
#include <i2c_driver_wire.h>
#include "Watchdog_t4.h"
WDT_T4<WDT1> wdt;



//*********************************** SETUP ************************************
//******************************************************************************
void setup() {
  Serial.begin (12000000); delay(100); // Teensy 4.0 always runs at 12 Mbit/sec
  Serial.print("Code revision: "); Serial.print(software_version);  Serial.println(".  Date: 09.011.22.  Entering Setup...");


  pinMode(pINT_module, OUTPUT);
  digitalWrite(pINT_module, HIGH);  // module interrupt pin is default high.

  // SETTING UP WATCHDOG
  WDT_timings_t config;
  config.trigger = 2; // seconds before timeout that callback function is triggered
  config.timeout = dogTimeout; // in seconds, 0->128
  config.callback = myCallback;
  wdt.begin(config);

  pinMode(13, OUTPUT); // SCK

  // setting up MAX35 registers
#ifdef simulation_mode
  MAXSetup();
  Serial.println();  Serial.println();
  Serial.println("********************************************************");
  Serial.println("******** MODULE HAS BOOTED INTO SIMULATION MODE ********");
  Serial.println("********************************************************");
  Serial.println();
#else
  while (MAXSetup() == LOW) {} // hardware wont work without this succeeding, so keep trying until it does...
#endif

  // SETTING UP EPPROM STUFF
  //clearEEPROM();
  //defaultCalibration();
  //printEEPROM();
  EEPROM_initialisation();
  loadCalibrationEEPROM(); // load calibration from flash into memory
  calibrationSummary();
  //printEEPROM();

  //calibrate_comparators();
  //baselineTOF(); // calibrate fixed zero-flow baseline
  //saveCalibration();  // save calibration from memory to EEPROM
#ifdef GENDebug
  calibrationSummary();
#endif


  // SETTING UP I2C
  Wire.begin(I2C_Address);
  Wire.onReceive(receiveInstruction);  // interrupt handler for incoming messages
  Wire.onRequest(dataRequest);  // interrupt handler for when data is wanted


  bitWrite(statusReg, POR, HIGH);
  bitWrite(statusReg, awaiting, HIGH);
  digitalWrite(pINT_module, LOW); // assert int pin
  // setup is complete and the module now idle

}//******************************** EMD OF SETUP ********************************



//******************************************************************************
//********************************* MAIN LOOP **********************************
//******************************************************************************
void loop() {
  wdt.feed(); // restart the countdown for watchdog forced reset
  bitWrite(statusReg, awaiting, HIGH);  // module idle

  if (bSerialControl != 0) { // REMAIN IN SERIAL CONTROL UNTIL DISABLED
    serialControl();
  }


  // this reboots the flow module if there has been no comms via I2C for some time
    static uint32_t mainIdle = 0;
    float idleTime = (float)(millis() - mainIdle) / 1000;
    //Serial.print("Module idle for ");  Serial.print(idleTime); Serial.println(" seconds");
    //delay(10);
    if (idleTime >= 60) { // in seconds
  #ifdef errorDebug
      Serial.println();  Serial.println();
      Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      Serial.println("!!  MODULE HAS BEEN IDLE 60 SECONDS... Rebooting...  !!");
      Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  #endif
      reboot();
    }


#ifdef GENDebug
  if (bIdleFlag == HIGH) {
    Serial.println(); Serial.println();
    Serial.println("Flow Module awaiting instruction... ");
    printStatus(); // prints the status register, and trnslates it into english.
    bIdleFlag = LOW; // don't repeat this.
  }
#endif


  // I2C RECIEVED AN INSTRUCTION
  if (I2C_instructionRecieved > 0) {  // did we just recieve some bytes?
    bitWrite(statusReg, awaiting, LOW); // module busy
    if (readInstruction() == 0) // If we've recieved just one byte...
      actionOpcode(); // just 1 byte means a command
    else
      configChange(); // more than 1 byte means config change
    I2C_instructionRecieved = 0; // Reset to 0 bytes (no commands or data waiting)
    bIdleFlag = HIGH; // module is idle again
    mainIdle = millis();
  }


  // TOF MEASUREMENT COMMANDED
  if (bStartTOF == HIGH) {
    bitWrite(statusReg, awaiting, LOW);  // module busy
    measureFlow();
    bIdleFlag = HIGH; // module is idle again
  }

// if there is no controller to initiate measurements the below can be used instead.
  //#ifdef simulation_mode
  //    measureFlow();
  //    delay(3000);
  //#endif

} //***************************** END OF MAIN LOOP *****************************



void measureFlow() {
#ifdef GENDebug
  Serial.println();  Serial.println();  Serial.println();  Serial.println();
  Serial.println("Flow measurement starting...");
#endif
  bitWrite(statusReg, TOFprog, HIGH);  // measuring flow
  digitalWrite(pINT_module, LOW); // assert int pin


  // pretty much everything here is also temperature corrected.
  // density --> sound-speed --> propagation length (+TOF) --> uncorrected flow --> viscosity --> Reynolds num --> corrected flow --> all channels combined based on data quality
  measureTOF();
  calcSoundSpeed();
  calcFlow();
  Cha_combination();
  correct_flow();
//  if (combinedFlow > abs(flow_cutoff * 1000000)) { // don't accumulate on super-low (probably 0 real) flow.
  accumulate_flow();
//  }
//  else { // don't accumulate the flow. Instead...
    //accVolContinue = LOW; // if the flow is very low, then don't bother making up for downtime.
//  }

  //flowprev = flow;
  massFlow = density * combinedFlow; // litres per sec x KG/m^3 gives grams per second.
  bStartTOF = LOW;

  bitWrite(statusReg, TOFprog, LOW);  // not measuring flow
  bitWrite(statusReg, TOFdone, HIGH); // flow measurement done
  digitalWrite(pINT_module, LOW); // assert int pin

  //printEEPROM();

#ifdef ToFDebug
  printTOF(); // print results to serial
#endif
}




//********************************* standardDev *********************************
//*******************************************************************************
// This function accepts an array of data and it's length, and returns the standard deviation.
// This function is resistant to very large arrays of very large values.
uint32_t standardDev(uint32_t data[], uint16_t dataSize) {
  uint16_t N = dataSize - 1;
  double   mean = 0;
  double   sum_sqDiff = 0;
  double   stdDev = 0;

  for (int i = 0; i < dataSize; i++) {
    mean = mean + (data[i] / dataSize);
  }
  for (int i = 0; i < dataSize; i++) {
    sum_sqDiff = sum_sqDiff + sq( (data[i] / N) - (mean / N) );
  }
  stdDev = pow(N * sum_sqDiff, 0.5);
  return stdDev; // send back the standard deviation
}
// SIGNED VERSION
uint32_t standardDev_signed(int32_t data[], uint16_t dataSize) {
  uint16_t N = dataSize - 1;
  double   mean = 0;
  double   sum_sqDiff = 0;
  double   stdDev = 0;

  for (int i = 0; i < dataSize; i++) {
    mean = mean + (data[i] / dataSize);
  }
  for (int i = 0; i < dataSize; i++) {
    sum_sqDiff = sum_sqDiff + sq( (data[i] / N) - (mean / N) );
  }
  stdDev = pow(N * sum_sqDiff, 0.5);
  return stdDev; // send back the standard deviation
} //****************************** END OF standardDev ******************************


void printStatus() {
  Serial.print("Status is: ");
  printBIN16(statusReg);

  if (bitRead(statusReg, 15) == HIGH)
    Serial.print(" - !! invalid opcode !! ");
  if (bitRead(statusReg, 14) == HIGH)
    Serial.print(" - !! Issue sending data !! ");
  if (bitRead(statusReg, 13) == HIGH)
    Serial.print(" - Requested data ready to read ");
  if (bitRead(statusReg, 12) == HIGH)
    Serial.print(" - !! TOF measurement issue !! ");
  if (bitRead(statusReg, 11) == HIGH)
    Serial.print(" - TOF measurement completed ");
  if (bitRead(statusReg, 10) == HIGH)
    Serial.print(" - TOF measurement in progress ");
  if (bitRead(statusReg, 9) == HIGH)
    Serial.print(" - !! Baseline / calibration issue !! ");
  if (bitRead(statusReg, 8) == HIGH)
    Serial.print(" - Baseline / calibration finished ");
  if (bitRead(statusReg, 7) == HIGH)
    Serial.print(" - !! Config issue !! ");
  if (bitRead(statusReg, 6) == HIGH)
    Serial.print(" - Config success ");
  if (bitRead(statusReg, 5) == HIGH)
    Serial.print(" - Awaiting / idle ");
  if (bitRead(statusReg, 4) == HIGH)
    Serial.print(" - Power on reset ");
  if (bitRead(statusReg, 3) == HIGH)
    Serial.print(" - Channel 4 ");
  if (bitRead(statusReg, 2) == HIGH)
    Serial.print(" - Channel 3 ");
  if (bitRead(statusReg, 1) == HIGH)
    Serial.print(" - Channel 2 ");
  if (bitRead(statusReg, 0) == HIGH)
    Serial.print(" - Channel 1 ");
  Serial.println();
}


void printDouble( double val, byte precision) {
  // prints val with number of decimal places determine by precision
  // precision is a number from 0 to 6 indicating the desired decimial places
  // example: printDouble( 3.1415, 2); // prints 3.14 (two decimal places)

  Serial.print (int(val));  //prints the int part
  if ( precision > 0) {
    Serial.print("."); // print the decimal point
    unsigned long frac;
    unsigned long mult = 1;
    byte padding = precision - 1;
    while (precision--)
      mult *= 10;

    if (val >= 0)
      frac = (val - int(val)) * mult;
    else
      frac = (int(val) - val ) * mult;
    unsigned long frac1 = frac;
    while ( frac1 /= 10 )
      padding--;
    while (  padding--)
      Serial.print("0");
    Serial.print(frac, DEC) ;
  }
}


void myCallback() {
  if (bSerialControl == 1) {
    wdt.feed();
  }
#ifdef errorDebug
  else {
    Serial.println();  Serial.println();  Serial.println();
    Serial.println("********************************************************");
    Serial.println("!! FIRMWARE CRASH SUSPECTED - RESTARTING IN 2 SECONDS !!");
    Serial.println("********************************************************");
  }
#endif
}


// forces a reboot via the watchdog
void reboot() {
  WDT_timings_t config;
  config.trigger = 0; // seconds before timeout that callback function is triggered
  config.timeout = 0; // in seconds, 0->128
  //config.callback = myCallback;
  wdt.begin(config);
  while (1 == 1); // infinite delay
}
